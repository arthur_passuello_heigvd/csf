set NumericStdNoWarnings 1

file delete -force "work"
file mkdir "work"

vlib work
vmap work work

vcom -2008 -explicit -novopt ../src_vhdl/log_pkg.vhd
vcom -2008 -explicit -novopt ../src_vhdl/rom.vhd
vcom -2008 -explicit -novopt ../src_vhdl/generator.vhd
vcom -2008 -explicit -novopt ../src_tb/generator_tb.vhd

vsim -novopt generator_tb

add wave -r *

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

run 10 ns

set StdArithNoWarnings 0
set NumericStdNoWarnings 0

run 1000000 ns
