-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : generator.vhd
-- Description  :
--
-- Author       : Mike Meury
-- Date         : 11.09.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    26.02.17           Creatione
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------------
-- Functions log package --
---------------------------
use work.log_pkg.all;

------------
-- Entity --
------------
entity generator is
    port (
        -- Standard inputs
        clk_i    : in  std_logic;
        rst_i    : in  std_logic;
        -- specifics inputs
        enable_i : in  std_logic;
        -- specifics outputs
        data_o   : out std_logic_vector(15 downto 0);  -- Output
        valid_o  : out std_logic
        );
end entity generator;

architecture Behavioral of generator is

    ---------------
    -- Constants --
    ---------------
    constant SAMPLING_CST : integer := 900;
    
    -------------
    -- Signals --
    -------------
    -- @27MHz => 30kHz
    -- counter have to count 900 times
    signal counter_sampling_s : unsigned(9 downto 0);  -- max 1024
    signal counter_rom_s      : unsigned(15 downto 0);
    -- addr
    signal addr_s             : std_logic_vector(15 downto 0);

begin

    -----------------------
    -- ROM instantiation --
    -----------------------
    rom0 : entity work.rom
        generic map (
            DATA_WIDTH => 16,
            ADDR_WIDTH => 16
            )
        port map (
            clk_i  => clk_i,
            addr_i => addr_s,
            data_o => data_o
            );

    -------------
    -- Control --
    -------------
    -- counter to respect sampling
    process (clk_i, rst_i)
    begin
        if rst_i = '1' then
            counter_sampling_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if counter_sampling_s = SAMPLING_CST-1 then
                counter_sampling_s <= (others => '0');
            else
                counter_sampling_s <= counter_sampling_s + 1;
            end if;
        end if;
    end process;

    -- counter to control data from rom
    process (clk_i, rst_i)
    begin
        if rst_i = '1' then
            counter_rom_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if counter_sampling_s = SAMPLING_CST-1 then
                counter_rom_s <= counter_rom_s + 1;
            end if;
        end if;
    end process;

    -- set address for rom
    addr_s <= std_logic_vector(counter_rom_s);

    -- set valid in a random moment during data stable
    -- counter = 1 because ROM need 1 clock to give data
    valid_o <= '1' when counter_sampling_s = 1 and enable_i = '1' else
               '0';

end architecture Behavioral;
