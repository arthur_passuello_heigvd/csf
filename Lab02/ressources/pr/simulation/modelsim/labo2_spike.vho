-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Standard Edition"

-- DATE "03/26/2018 19:24:30"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for QuestaSim (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	DE1_SOC_golden_top IS
    PORT (
	CLOCK_50 : IN std_logic;
	TD_CLK27 : IN std_logic;
	GPIO_0 : BUFFER std_logic_vector(35 DOWNTO 0);
	GPIO_1 : BUFFER std_logic_vector(35 DOWNTO 0);
	HEX0 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX1 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX2 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX3 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX4 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX5 : BUFFER std_logic_vector(6 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0);
	LEDR : BUFFER std_logic_vector(9 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	VGA_B : BUFFER std_logic_vector(7 DOWNTO 0);
	VGA_BLANK_N : BUFFER std_logic;
	VGA_CLK : BUFFER std_logic;
	VGA_G : BUFFER std_logic_vector(7 DOWNTO 0);
	VGA_HS : BUFFER std_logic;
	VGA_R : BUFFER std_logic_vector(7 DOWNTO 0);
	VGA_SYNC_N : BUFFER std_logic;
	VGA_VS : BUFFER std_logic
	);
END DE1_SOC_golden_top;

-- Design Ports Information
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_AE26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[1]	=>  Location: PIN_AE27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[2]	=>  Location: PIN_AE28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[3]	=>  Location: PIN_AG27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[4]	=>  Location: PIN_AF28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[5]	=>  Location: PIN_AG28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[6]	=>  Location: PIN_AH28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[0]	=>  Location: PIN_AJ29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[1]	=>  Location: PIN_AH29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[2]	=>  Location: PIN_AH30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[3]	=>  Location: PIN_AG30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[4]	=>  Location: PIN_AF29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[5]	=>  Location: PIN_AF30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[6]	=>  Location: PIN_AD27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[1]	=>  Location: PIN_AE29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[2]	=>  Location: PIN_AD29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[3]	=>  Location: PIN_AC28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[4]	=>  Location: PIN_AD30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[5]	=>  Location: PIN_AC29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[6]	=>  Location: PIN_AC30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[0]	=>  Location: PIN_AD26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[1]	=>  Location: PIN_AC27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[2]	=>  Location: PIN_AD25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[3]	=>  Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[4]	=>  Location: PIN_AB28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[5]	=>  Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[6]	=>  Location: PIN_AB22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[0]	=>  Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[1]	=>  Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[2]	=>  Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[3]	=>  Location: PIN_W22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[4]	=>  Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[5]	=>  Location: PIN_V23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[6]	=>  Location: PIN_W25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[0]	=>  Location: PIN_V25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[1]	=>  Location: PIN_AA28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[2]	=>  Location: PIN_Y27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[3]	=>  Location: PIN_AB27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[4]	=>  Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[5]	=>  Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[6]	=>  Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- SW[0]	=>  Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AF9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AD11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AD12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AE11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_AC9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_AD10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_AE12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- VGA_B[0]	=>  Location: PIN_B13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[1]	=>  Location: PIN_G13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[2]	=>  Location: PIN_H13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[3]	=>  Location: PIN_F14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[4]	=>  Location: PIN_H14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[5]	=>  Location: PIN_F15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[6]	=>  Location: PIN_G15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_B[7]	=>  Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_SYNC_N	=>  Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_BLANK_N	=>  Location: PIN_F10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_CLK	=>  Location: PIN_A11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[0]	=>  Location: PIN_J9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[1]	=>  Location: PIN_J10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[2]	=>  Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[3]	=>  Location: PIN_G10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[4]	=>  Location: PIN_G11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[5]	=>  Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[6]	=>  Location: PIN_F11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_G[7]	=>  Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_HS	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[0]	=>  Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[1]	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[2]	=>  Location: PIN_E13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[3]	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[4]	=>  Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[5]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[6]	=>  Location: PIN_E12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_R[7]	=>  Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- VGA_VS	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[0]	=>  Location: PIN_AC18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[1]	=>  Location: PIN_Y17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[2]	=>  Location: PIN_AD17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[3]	=>  Location: PIN_Y18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[4]	=>  Location: PIN_AK16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[5]	=>  Location: PIN_AK18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[6]	=>  Location: PIN_AK19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[7]	=>  Location: PIN_AJ19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[8]	=>  Location: PIN_AJ17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[9]	=>  Location: PIN_AJ16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[10]	=>  Location: PIN_AH18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[11]	=>  Location: PIN_AH17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[12]	=>  Location: PIN_AG16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[13]	=>  Location: PIN_AE16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[14]	=>  Location: PIN_AF16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[15]	=>  Location: PIN_AG17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[16]	=>  Location: PIN_AA18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[17]	=>  Location: PIN_AA19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[18]	=>  Location: PIN_AE17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[19]	=>  Location: PIN_AC20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[20]	=>  Location: PIN_AH19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[21]	=>  Location: PIN_AJ20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[22]	=>  Location: PIN_AH20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[23]	=>  Location: PIN_AK21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[24]	=>  Location: PIN_AD19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[25]	=>  Location: PIN_AD20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[26]	=>  Location: PIN_AE18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[27]	=>  Location: PIN_AE19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[28]	=>  Location: PIN_AF20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[29]	=>  Location: PIN_AF21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[30]	=>  Location: PIN_AF19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[31]	=>  Location: PIN_AG21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[32]	=>  Location: PIN_AF18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[33]	=>  Location: PIN_AG20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[34]	=>  Location: PIN_AG18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_0[35]	=>  Location: PIN_AJ21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[0]	=>  Location: PIN_AB17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[1]	=>  Location: PIN_AA21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[2]	=>  Location: PIN_AB21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[3]	=>  Location: PIN_AC23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[4]	=>  Location: PIN_AD24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[5]	=>  Location: PIN_AE23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[6]	=>  Location: PIN_AE24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[7]	=>  Location: PIN_AF25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[8]	=>  Location: PIN_AF26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[9]	=>  Location: PIN_AG25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[10]	=>  Location: PIN_AG26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[11]	=>  Location: PIN_AH24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[12]	=>  Location: PIN_AH27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[13]	=>  Location: PIN_AJ27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[14]	=>  Location: PIN_AK29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[15]	=>  Location: PIN_AK28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[16]	=>  Location: PIN_AK27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[17]	=>  Location: PIN_AJ26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[18]	=>  Location: PIN_AK26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[19]	=>  Location: PIN_AH25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[20]	=>  Location: PIN_AJ25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[21]	=>  Location: PIN_AJ24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[22]	=>  Location: PIN_AK24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[23]	=>  Location: PIN_AG23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[24]	=>  Location: PIN_AK23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[25]	=>  Location: PIN_AH23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[26]	=>  Location: PIN_AK22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[27]	=>  Location: PIN_AJ22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[28]	=>  Location: PIN_AH22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[29]	=>  Location: PIN_AG22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[30]	=>  Location: PIN_AF24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[31]	=>  Location: PIN_AF23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[32]	=>  Location: PIN_AE22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[33]	=>  Location: PIN_AD21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[34]	=>  Location: PIN_AA20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- GPIO_1[35]	=>  Location: PIN_AC22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- TD_CLK27	=>  Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF DE1_SOC_golden_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_TD_CLK27 : std_logic;
SIGNAL ww_GPIO_0 : std_logic_vector(35 DOWNTO 0);
SIGNAL ww_GPIO_1 : std_logic_vector(35 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX4 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX5 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_B : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_BLANK_N : std_logic;
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL ww_VGA_G : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_R : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_SYNC_N : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \GPIO_0[0]~input_o\ : std_logic;
SIGNAL \GPIO_0[1]~input_o\ : std_logic;
SIGNAL \GPIO_0[2]~input_o\ : std_logic;
SIGNAL \GPIO_0[3]~input_o\ : std_logic;
SIGNAL \GPIO_0[4]~input_o\ : std_logic;
SIGNAL \GPIO_0[5]~input_o\ : std_logic;
SIGNAL \GPIO_0[6]~input_o\ : std_logic;
SIGNAL \GPIO_0[7]~input_o\ : std_logic;
SIGNAL \GPIO_0[8]~input_o\ : std_logic;
SIGNAL \GPIO_0[9]~input_o\ : std_logic;
SIGNAL \GPIO_0[10]~input_o\ : std_logic;
SIGNAL \GPIO_0[11]~input_o\ : std_logic;
SIGNAL \GPIO_0[12]~input_o\ : std_logic;
SIGNAL \GPIO_0[13]~input_o\ : std_logic;
SIGNAL \GPIO_0[14]~input_o\ : std_logic;
SIGNAL \GPIO_0[15]~input_o\ : std_logic;
SIGNAL \GPIO_0[16]~input_o\ : std_logic;
SIGNAL \GPIO_0[17]~input_o\ : std_logic;
SIGNAL \GPIO_0[18]~input_o\ : std_logic;
SIGNAL \GPIO_0[19]~input_o\ : std_logic;
SIGNAL \GPIO_0[20]~input_o\ : std_logic;
SIGNAL \GPIO_0[21]~input_o\ : std_logic;
SIGNAL \GPIO_0[22]~input_o\ : std_logic;
SIGNAL \GPIO_0[23]~input_o\ : std_logic;
SIGNAL \GPIO_0[24]~input_o\ : std_logic;
SIGNAL \GPIO_0[25]~input_o\ : std_logic;
SIGNAL \GPIO_0[26]~input_o\ : std_logic;
SIGNAL \GPIO_0[27]~input_o\ : std_logic;
SIGNAL \GPIO_0[28]~input_o\ : std_logic;
SIGNAL \GPIO_0[29]~input_o\ : std_logic;
SIGNAL \GPIO_0[30]~input_o\ : std_logic;
SIGNAL \GPIO_0[31]~input_o\ : std_logic;
SIGNAL \GPIO_0[32]~input_o\ : std_logic;
SIGNAL \GPIO_0[33]~input_o\ : std_logic;
SIGNAL \GPIO_0[34]~input_o\ : std_logic;
SIGNAL \GPIO_0[35]~input_o\ : std_logic;
SIGNAL \GPIO_1[0]~input_o\ : std_logic;
SIGNAL \GPIO_1[1]~input_o\ : std_logic;
SIGNAL \GPIO_1[2]~input_o\ : std_logic;
SIGNAL \GPIO_1[3]~input_o\ : std_logic;
SIGNAL \GPIO_1[4]~input_o\ : std_logic;
SIGNAL \GPIO_1[5]~input_o\ : std_logic;
SIGNAL \GPIO_1[6]~input_o\ : std_logic;
SIGNAL \GPIO_1[7]~input_o\ : std_logic;
SIGNAL \GPIO_1[8]~input_o\ : std_logic;
SIGNAL \GPIO_1[9]~input_o\ : std_logic;
SIGNAL \GPIO_1[10]~input_o\ : std_logic;
SIGNAL \GPIO_1[11]~input_o\ : std_logic;
SIGNAL \GPIO_1[12]~input_o\ : std_logic;
SIGNAL \GPIO_1[13]~input_o\ : std_logic;
SIGNAL \GPIO_1[14]~input_o\ : std_logic;
SIGNAL \GPIO_1[15]~input_o\ : std_logic;
SIGNAL \GPIO_1[16]~input_o\ : std_logic;
SIGNAL \GPIO_1[17]~input_o\ : std_logic;
SIGNAL \GPIO_1[18]~input_o\ : std_logic;
SIGNAL \GPIO_1[19]~input_o\ : std_logic;
SIGNAL \GPIO_1[20]~input_o\ : std_logic;
SIGNAL \GPIO_1[21]~input_o\ : std_logic;
SIGNAL \GPIO_1[22]~input_o\ : std_logic;
SIGNAL \GPIO_1[23]~input_o\ : std_logic;
SIGNAL \GPIO_1[24]~input_o\ : std_logic;
SIGNAL \GPIO_1[25]~input_o\ : std_logic;
SIGNAL \GPIO_1[26]~input_o\ : std_logic;
SIGNAL \GPIO_1[27]~input_o\ : std_logic;
SIGNAL \GPIO_1[28]~input_o\ : std_logic;
SIGNAL \GPIO_1[29]~input_o\ : std_logic;
SIGNAL \GPIO_1[30]~input_o\ : std_logic;
SIGNAL \GPIO_1[31]~input_o\ : std_logic;
SIGNAL \GPIO_1[32]~input_o\ : std_logic;
SIGNAL \GPIO_1[33]~input_o\ : std_logic;
SIGNAL \GPIO_1[34]~input_o\ : std_logic;
SIGNAL \GPIO_1[35]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \TD_CLK27~input_o\ : std_logic;
SIGNAL \TD_CLK27~inputCLKENA0_outclk\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~41_sumout\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~38\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~33_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|H_Cont[2]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~34\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~29_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~30\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~25_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~26\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~9_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~10\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~5_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~6\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~1_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~2\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~13_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~14\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~17_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~18\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~21_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|LessThan2~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~42\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add4~37_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Equal0~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_BLANK~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_HS~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_HS~q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~37_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~2\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~25_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~26\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~29_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~30\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~33_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~34\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~17_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~18\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~21_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|LessThan1~3_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|LessThan1~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|LessThan3~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~38\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~41_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~42\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~9_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~10\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~13_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~14\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~5_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~6\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add5~1_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_BLANK~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|LessThan1~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_BLANK~2_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|V_Cont[3]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~22\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~18\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~26\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~1_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~21_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~17_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|LessThan1~2_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|V_Cont[1]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oCurrent_Y[0]~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|V_Cont[6]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~2\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~9_sumout\ : std_logic;
SIGNAL \display_inst|Add0~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~25_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oCurrent_Y[4]~2_combout\ : std_logic;
SIGNAL \display_inst|Mux0~0_combout\ : std_logic;
SIGNAL \display_inst|Mux0~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~10\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~5_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oCurrent_Y[6]~0_combout\ : std_logic;
SIGNAL \display_inst|Mux0~2_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~6\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Add3~13_sumout\ : std_logic;
SIGNAL \display_inst|Mux0~3_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|Equal2~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_VS~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|oVGA_VS~q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|H_Cont\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \VGA_Ctrl_inst|V_Cont\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \VGA_Ctrl_inst|ALT_INV_H_Cont[2]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_V_Cont[1]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_V_Cont[6]~DUPLICATE_q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_V_Cont[3]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_TD_CLK27~inputCLKENA0_outclk\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Equal2~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_LessThan1~3_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oVGA_VS~q\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\ : std_logic;
SIGNAL \display_inst|ALT_INV_Mux0~2_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[7]~4_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[5]~3_combout\ : std_logic;
SIGNAL \display_inst|ALT_INV_Mux0~1_combout\ : std_logic;
SIGNAL \display_inst|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[4]~2_combout\ : std_logic;
SIGNAL \display_inst|ALT_INV_Add0~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[0]~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[6]~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_LessThan1~2_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~2_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_H_Cont\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~25_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_V_Cont\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~21_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~17_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~13_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~9_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~5_sumout\ : std_logic;
SIGNAL \VGA_Ctrl_inst|ALT_INV_Add3~1_sumout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_TD_CLK27 <= TD_CLK27;
GPIO_0 <= ww_GPIO_0;
GPIO_1 <= ww_GPIO_1;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
ww_SW <= SW;
VGA_B <= ww_VGA_B;
VGA_BLANK_N <= ww_VGA_BLANK_N;
VGA_CLK <= ww_VGA_CLK;
VGA_G <= ww_VGA_G;
VGA_HS <= ww_VGA_HS;
VGA_R <= ww_VGA_R;
VGA_SYNC_N <= ww_VGA_SYNC_N;
VGA_VS <= ww_VGA_VS;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\VGA_Ctrl_inst|ALT_INV_H_Cont[2]~DUPLICATE_q\ <= NOT \VGA_Ctrl_inst|H_Cont[2]~DUPLICATE_q\;
\VGA_Ctrl_inst|ALT_INV_V_Cont[1]~DUPLICATE_q\ <= NOT \VGA_Ctrl_inst|V_Cont[1]~DUPLICATE_q\;
\VGA_Ctrl_inst|ALT_INV_V_Cont[6]~DUPLICATE_q\ <= NOT \VGA_Ctrl_inst|V_Cont[6]~DUPLICATE_q\;
\VGA_Ctrl_inst|ALT_INV_V_Cont[3]~DUPLICATE_q\ <= NOT \VGA_Ctrl_inst|V_Cont[3]~DUPLICATE_q\;
\ALT_INV_TD_CLK27~inputCLKENA0_outclk\ <= NOT \TD_CLK27~inputCLKENA0_outclk\;
\VGA_Ctrl_inst|ALT_INV_Equal2~0_combout\ <= NOT \VGA_Ctrl_inst|Equal2~0_combout\;
\VGA_Ctrl_inst|ALT_INV_Equal0~0_combout\ <= NOT \VGA_Ctrl_inst|Equal0~0_combout\;
\VGA_Ctrl_inst|ALT_INV_LessThan1~3_combout\ <= NOT \VGA_Ctrl_inst|LessThan1~3_combout\;
\VGA_Ctrl_inst|ALT_INV_oVGA_VS~q\ <= NOT \VGA_Ctrl_inst|oVGA_VS~q\;
\VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\ <= NOT \VGA_Ctrl_inst|oVGA_HS~q\;
\display_inst|ALT_INV_Mux0~2_combout\ <= NOT \display_inst|Mux0~2_combout\;
\VGA_Ctrl_inst|ALT_INV_oCurrent_Y[7]~4_combout\ <= NOT \VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\;
\VGA_Ctrl_inst|ALT_INV_oCurrent_Y[5]~3_combout\ <= NOT \VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\;
\display_inst|ALT_INV_Mux0~1_combout\ <= NOT \display_inst|Mux0~1_combout\;
\display_inst|ALT_INV_Mux0~0_combout\ <= NOT \display_inst|Mux0~0_combout\;
\VGA_Ctrl_inst|ALT_INV_oCurrent_Y[4]~2_combout\ <= NOT \VGA_Ctrl_inst|oCurrent_Y[4]~2_combout\;
\display_inst|ALT_INV_Add0~0_combout\ <= NOT \display_inst|Add0~0_combout\;
\VGA_Ctrl_inst|ALT_INV_oCurrent_Y[0]~1_combout\ <= NOT \VGA_Ctrl_inst|oCurrent_Y[0]~1_combout\;
\VGA_Ctrl_inst|ALT_INV_oCurrent_Y[6]~0_combout\ <= NOT \VGA_Ctrl_inst|oCurrent_Y[6]~0_combout\;
\VGA_Ctrl_inst|ALT_INV_LessThan1~2_combout\ <= NOT \VGA_Ctrl_inst|LessThan1~2_combout\;
\VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~2_combout\ <= NOT \VGA_Ctrl_inst|oVGA_BLANK~2_combout\;
\VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~1_combout\ <= NOT \VGA_Ctrl_inst|oVGA_BLANK~1_combout\;
\VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~0_combout\ <= NOT \VGA_Ctrl_inst|oVGA_BLANK~0_combout\;
\VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\ <= NOT \VGA_Ctrl_inst|LessThan1~1_combout\;
\VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\ <= NOT \VGA_Ctrl_inst|LessThan1~0_combout\;
\VGA_Ctrl_inst|ALT_INV_H_Cont\(0) <= NOT \VGA_Ctrl_inst|H_Cont\(0);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(1) <= NOT \VGA_Ctrl_inst|H_Cont\(1);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(2) <= NOT \VGA_Ctrl_inst|H_Cont\(2);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(3) <= NOT \VGA_Ctrl_inst|H_Cont\(3);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(4) <= NOT \VGA_Ctrl_inst|H_Cont\(4);
\VGA_Ctrl_inst|ALT_INV_Add3~25_sumout\ <= NOT \VGA_Ctrl_inst|Add3~25_sumout\;
\VGA_Ctrl_inst|ALT_INV_V_Cont\(1) <= NOT \VGA_Ctrl_inst|V_Cont\(1);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(0) <= NOT \VGA_Ctrl_inst|V_Cont\(0);
\VGA_Ctrl_inst|ALT_INV_Add3~21_sumout\ <= NOT \VGA_Ctrl_inst|Add3~21_sumout\;
\VGA_Ctrl_inst|ALT_INV_Add3~17_sumout\ <= NOT \VGA_Ctrl_inst|Add3~17_sumout\;
\VGA_Ctrl_inst|ALT_INV_Add3~13_sumout\ <= NOT \VGA_Ctrl_inst|Add3~13_sumout\;
\VGA_Ctrl_inst|ALT_INV_Add3~9_sumout\ <= NOT \VGA_Ctrl_inst|Add3~9_sumout\;
\VGA_Ctrl_inst|ALT_INV_Add3~5_sumout\ <= NOT \VGA_Ctrl_inst|Add3~5_sumout\;
\VGA_Ctrl_inst|ALT_INV_Add3~1_sumout\ <= NOT \VGA_Ctrl_inst|Add3~1_sumout\;
\VGA_Ctrl_inst|ALT_INV_H_Cont\(10) <= NOT \VGA_Ctrl_inst|H_Cont\(10);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(9) <= NOT \VGA_Ctrl_inst|H_Cont\(9);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(8) <= NOT \VGA_Ctrl_inst|H_Cont\(8);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(5) <= NOT \VGA_Ctrl_inst|H_Cont\(5);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(6) <= NOT \VGA_Ctrl_inst|H_Cont\(6);
\VGA_Ctrl_inst|ALT_INV_H_Cont\(7) <= NOT \VGA_Ctrl_inst|H_Cont\(7);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(8) <= NOT \VGA_Ctrl_inst|V_Cont\(8);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(7) <= NOT \VGA_Ctrl_inst|V_Cont\(7);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(6) <= NOT \VGA_Ctrl_inst|V_Cont\(6);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(10) <= NOT \VGA_Ctrl_inst|V_Cont\(10);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(9) <= NOT \VGA_Ctrl_inst|V_Cont\(9);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(3) <= NOT \VGA_Ctrl_inst|V_Cont\(3);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(2) <= NOT \VGA_Ctrl_inst|V_Cont\(2);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(4) <= NOT \VGA_Ctrl_inst|V_Cont\(4);
\VGA_Ctrl_inst|ALT_INV_V_Cont\(5) <= NOT \VGA_Ctrl_inst|V_Cont\(5);

-- Location: IOOBUF_X89_Y8_N39
\HEX0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(0));

-- Location: IOOBUF_X89_Y11_N79
\HEX0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(1));

-- Location: IOOBUF_X89_Y11_N96
\HEX0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(2));

-- Location: IOOBUF_X89_Y4_N79
\HEX0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(3));

-- Location: IOOBUF_X89_Y13_N56
\HEX0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(4));

-- Location: IOOBUF_X89_Y13_N39
\HEX0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(5));

-- Location: IOOBUF_X89_Y4_N96
\HEX0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX0(6));

-- Location: IOOBUF_X89_Y6_N39
\HEX1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(0));

-- Location: IOOBUF_X89_Y6_N56
\HEX1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(1));

-- Location: IOOBUF_X89_Y16_N39
\HEX1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(2));

-- Location: IOOBUF_X89_Y16_N56
\HEX1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(3));

-- Location: IOOBUF_X89_Y15_N39
\HEX1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(4));

-- Location: IOOBUF_X89_Y15_N56
\HEX1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(5));

-- Location: IOOBUF_X89_Y8_N56
\HEX1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX1(6));

-- Location: IOOBUF_X89_Y9_N22
\HEX2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(0));

-- Location: IOOBUF_X89_Y23_N39
\HEX2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(1));

-- Location: IOOBUF_X89_Y23_N56
\HEX2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(2));

-- Location: IOOBUF_X89_Y20_N79
\HEX2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(3));

-- Location: IOOBUF_X89_Y25_N39
\HEX2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(4));

-- Location: IOOBUF_X89_Y20_N96
\HEX2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(5));

-- Location: IOOBUF_X89_Y25_N56
\HEX2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(6));

-- Location: IOOBUF_X89_Y16_N5
\HEX3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(0));

-- Location: IOOBUF_X89_Y16_N22
\HEX3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(1));

-- Location: IOOBUF_X89_Y4_N45
\HEX3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(2));

-- Location: IOOBUF_X89_Y4_N62
\HEX3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(3));

-- Location: IOOBUF_X89_Y21_N39
\HEX3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(4));

-- Location: IOOBUF_X89_Y11_N62
\HEX3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(5));

-- Location: IOOBUF_X89_Y9_N5
\HEX3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(6));

-- Location: IOOBUF_X89_Y11_N45
\HEX4[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(0));

-- Location: IOOBUF_X89_Y13_N5
\HEX4[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(1));

-- Location: IOOBUF_X89_Y13_N22
\HEX4[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(2));

-- Location: IOOBUF_X89_Y8_N22
\HEX4[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(3));

-- Location: IOOBUF_X89_Y15_N22
\HEX4[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(4));

-- Location: IOOBUF_X89_Y15_N5
\HEX4[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(5));

-- Location: IOOBUF_X89_Y20_N45
\HEX4[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(6));

-- Location: IOOBUF_X89_Y20_N62
\HEX5[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(0));

-- Location: IOOBUF_X89_Y21_N56
\HEX5[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(1));

-- Location: IOOBUF_X89_Y25_N22
\HEX5[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(2));

-- Location: IOOBUF_X89_Y23_N22
\HEX5[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(3));

-- Location: IOOBUF_X89_Y9_N56
\HEX5[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(4));

-- Location: IOOBUF_X89_Y23_N5
\HEX5[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(5));

-- Location: IOOBUF_X89_Y9_N39
\HEX5[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(6));

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOOBUF_X40_Y81_N36
\VGA_B[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(0));

-- Location: IOOBUF_X28_Y81_N19
\VGA_B[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(1));

-- Location: IOOBUF_X20_Y81_N2
\VGA_B[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(2));

-- Location: IOOBUF_X36_Y81_N19
\VGA_B[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(3));

-- Location: IOOBUF_X28_Y81_N2
\VGA_B[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(4));

-- Location: IOOBUF_X36_Y81_N2
\VGA_B[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(5));

-- Location: IOOBUF_X40_Y81_N19
\VGA_B[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(6));

-- Location: IOOBUF_X32_Y81_N19
\VGA_B[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_VGA_B(7));

-- Location: IOOBUF_X28_Y81_N36
\VGA_SYNC_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_VGA_SYNC_N);

-- Location: IOOBUF_X6_Y81_N19
\VGA_BLANK_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~2_combout\,
	devoe => ww_devoe,
	o => ww_VGA_BLANK_N);

-- Location: IOOBUF_X38_Y81_N36
\VGA_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_TD_CLK27~inputCLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_VGA_CLK);

-- Location: IOOBUF_X4_Y81_N19
\VGA_G[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(0));

-- Location: IOOBUF_X4_Y81_N2
\VGA_G[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(1));

-- Location: IOOBUF_X20_Y81_N19
\VGA_G[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(2));

-- Location: IOOBUF_X6_Y81_N2
\VGA_G[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(3));

-- Location: IOOBUF_X10_Y81_N59
\VGA_G[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(4));

-- Location: IOOBUF_X10_Y81_N42
\VGA_G[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(5));

-- Location: IOOBUF_X18_Y81_N42
\VGA_G[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(6));

-- Location: IOOBUF_X18_Y81_N59
\VGA_G[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(7));

-- Location: IOOBUF_X36_Y81_N53
\VGA_HS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	devoe => ww_devoe,
	o => ww_VGA_HS);

-- Location: IOOBUF_X40_Y81_N53
\VGA_R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(0));

-- Location: IOOBUF_X38_Y81_N2
\VGA_R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(1));

-- Location: IOOBUF_X26_Y81_N59
\VGA_R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(2));

-- Location: IOOBUF_X38_Y81_N19
\VGA_R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(3));

-- Location: IOOBUF_X36_Y81_N36
\VGA_R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(4));

-- Location: IOOBUF_X22_Y81_N19
\VGA_R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(5));

-- Location: IOOBUF_X22_Y81_N2
\VGA_R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(6));

-- Location: IOOBUF_X26_Y81_N42
\VGA_R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \display_inst|Mux0~3_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(7));

-- Location: IOOBUF_X34_Y81_N42
\VGA_VS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \VGA_Ctrl_inst|ALT_INV_oVGA_VS~q\,
	devoe => ww_devoe,
	o => ww_VGA_VS);

-- Location: IOOBUF_X64_Y0_N2
\GPIO_0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(0));

-- Location: IOOBUF_X68_Y0_N2
\GPIO_0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(1));

-- Location: IOOBUF_X64_Y0_N19
\GPIO_0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(2));

-- Location: IOOBUF_X72_Y0_N2
\GPIO_0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(3));

-- Location: IOOBUF_X54_Y0_N53
\GPIO_0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(4));

-- Location: IOOBUF_X58_Y0_N59
\GPIO_0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(5));

-- Location: IOOBUF_X60_Y0_N53
\GPIO_0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(6));

-- Location: IOOBUF_X60_Y0_N36
\GPIO_0[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(7));

-- Location: IOOBUF_X58_Y0_N42
\GPIO_0[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(8));

-- Location: IOOBUF_X54_Y0_N36
\GPIO_0[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(9));

-- Location: IOOBUF_X56_Y0_N53
\GPIO_0[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(10));

-- Location: IOOBUF_X56_Y0_N36
\GPIO_0[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(11));

-- Location: IOOBUF_X50_Y0_N76
\GPIO_0[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(12));

-- Location: IOOBUF_X52_Y0_N36
\GPIO_0[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(13));

-- Location: IOOBUF_X52_Y0_N53
\GPIO_0[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(14));

-- Location: IOOBUF_X50_Y0_N93
\GPIO_0[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(15));

-- Location: IOOBUF_X68_Y0_N19
\GPIO_0[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(16));

-- Location: IOOBUF_X72_Y0_N19
\GPIO_0[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(17));

-- Location: IOOBUF_X50_Y0_N42
\GPIO_0[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(18));

-- Location: IOOBUF_X76_Y0_N2
\GPIO_0[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(19));

-- Location: IOOBUF_X58_Y0_N93
\GPIO_0[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(20));

-- Location: IOOBUF_X62_Y0_N36
\GPIO_0[21]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(21));

-- Location: IOOBUF_X54_Y0_N19
\GPIO_0[22]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(22));

-- Location: IOOBUF_X68_Y0_N36
\GPIO_0[23]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(23));

-- Location: IOOBUF_X76_Y0_N19
\GPIO_0[24]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(24));

-- Location: IOOBUF_X82_Y0_N42
\GPIO_0[25]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(25));

-- Location: IOOBUF_X66_Y0_N42
\GPIO_0[26]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(26));

-- Location: IOOBUF_X66_Y0_N59
\GPIO_0[27]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(27));

-- Location: IOOBUF_X70_Y0_N2
\GPIO_0[28]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(28));

-- Location: IOOBUF_X70_Y0_N19
\GPIO_0[29]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(29));

-- Location: IOOBUF_X62_Y0_N2
\GPIO_0[30]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(30));

-- Location: IOOBUF_X54_Y0_N2
\GPIO_0[31]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(31));

-- Location: IOOBUF_X50_Y0_N59
\GPIO_0[32]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(32));

-- Location: IOOBUF_X62_Y0_N19
\GPIO_0[33]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(33));

-- Location: IOOBUF_X58_Y0_N76
\GPIO_0[34]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(34));

-- Location: IOOBUF_X62_Y0_N53
\GPIO_0[35]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_0(35));

-- Location: IOOBUF_X56_Y0_N19
\GPIO_1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(0));

-- Location: IOOBUF_X88_Y0_N3
\GPIO_1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(1));

-- Location: IOOBUF_X88_Y0_N20
\GPIO_1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(2));

-- Location: IOOBUF_X86_Y0_N19
\GPIO_1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(3));

-- Location: IOOBUF_X88_Y0_N37
\GPIO_1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(4));

-- Location: IOOBUF_X78_Y0_N19
\GPIO_1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(5));

-- Location: IOOBUF_X88_Y0_N54
\GPIO_1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(6));

-- Location: IOOBUF_X86_Y0_N36
\GPIO_1[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(7));

-- Location: IOOBUF_X86_Y0_N53
\GPIO_1[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(8));

-- Location: IOOBUF_X78_Y0_N36
\GPIO_1[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(9));

-- Location: IOOBUF_X84_Y0_N36
\GPIO_1[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(10));

-- Location: IOOBUF_X64_Y0_N53
\GPIO_1[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(11));

-- Location: IOOBUF_X84_Y0_N53
\GPIO_1[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(12));

-- Location: IOOBUF_X80_Y0_N36
\GPIO_1[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(13));

-- Location: IOOBUF_X82_Y0_N93
\GPIO_1[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(14));

-- Location: IOOBUF_X82_Y0_N76
\GPIO_1[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(15));

-- Location: IOOBUF_X80_Y0_N53
\GPIO_1[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(16));

-- Location: IOOBUF_X76_Y0_N36
\GPIO_1[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(17));

-- Location: IOOBUF_X76_Y0_N53
\GPIO_1[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(18));

-- Location: IOOBUF_X78_Y0_N53
\GPIO_1[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(19));

-- Location: IOOBUF_X74_Y0_N93
\GPIO_1[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(20));

-- Location: IOOBUF_X74_Y0_N76
\GPIO_1[21]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(21));

-- Location: IOOBUF_X72_Y0_N53
\GPIO_1[22]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(22));

-- Location: IOOBUF_X64_Y0_N36
\GPIO_1[23]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(23));

-- Location: IOOBUF_X72_Y0_N36
\GPIO_1[24]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(24));

-- Location: IOOBUF_X70_Y0_N36
\GPIO_1[25]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(25));

-- Location: IOOBUF_X68_Y0_N53
\GPIO_1[26]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(26));

-- Location: IOOBUF_X70_Y0_N53
\GPIO_1[27]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(27));

-- Location: IOOBUF_X66_Y0_N93
\GPIO_1[28]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(28));

-- Location: IOOBUF_X66_Y0_N76
\GPIO_1[29]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(29));

-- Location: IOOBUF_X74_Y0_N59
\GPIO_1[30]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(30));

-- Location: IOOBUF_X74_Y0_N42
\GPIO_1[31]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(31));

-- Location: IOOBUF_X78_Y0_N2
\GPIO_1[32]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(32));

-- Location: IOOBUF_X82_Y0_N59
\GPIO_1[33]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(33));

-- Location: IOOBUF_X84_Y0_N19
\GPIO_1[34]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(34));

-- Location: IOOBUF_X86_Y0_N2
\GPIO_1[35]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_GPIO_1(35));

-- Location: IOIBUF_X40_Y81_N1
\TD_CLK27~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TD_CLK27,
	o => \TD_CLK27~input_o\);

-- Location: CLKCTRL_G12
\TD_CLK27~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \TD_CLK27~input_o\,
	outclk => \TD_CLK27~inputCLKENA0_outclk\);

-- Location: LABCELL_X37_Y80_N0
\VGA_Ctrl_inst|Add4~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~41_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(0) ) + ( VCC ) + ( !VCC ))
-- \VGA_Ctrl_inst|Add4~42\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(0),
	cin => GND,
	sumout => \VGA_Ctrl_inst|Add4~41_sumout\,
	cout => \VGA_Ctrl_inst|Add4~42\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: LABCELL_X37_Y80_N3
\VGA_Ctrl_inst|Add4~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~37_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(1) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~42\ ))
-- \VGA_Ctrl_inst|Add4~38\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(1) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_H_Cont\(1),
	cin => \VGA_Ctrl_inst|Add4~42\,
	sumout => \VGA_Ctrl_inst|Add4~37_sumout\,
	cout => \VGA_Ctrl_inst|Add4~38\);

-- Location: LABCELL_X37_Y80_N6
\VGA_Ctrl_inst|Add4~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~33_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont[2]~DUPLICATE_q\ ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~38\ ))
-- \VGA_Ctrl_inst|Add4~34\ = CARRY(( \VGA_Ctrl_inst|H_Cont[2]~DUPLICATE_q\ ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont[2]~DUPLICATE_q\,
	cin => \VGA_Ctrl_inst|Add4~38\,
	sumout => \VGA_Ctrl_inst|Add4~33_sumout\,
	cout => \VGA_Ctrl_inst|Add4~34\);

-- Location: FF_X37_Y80_N7
\VGA_Ctrl_inst|H_Cont[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont[2]~DUPLICATE_q\);

-- Location: LABCELL_X37_Y80_N9
\VGA_Ctrl_inst|Add4~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~29_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(3) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~34\ ))
-- \VGA_Ctrl_inst|Add4~30\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(3) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(3),
	cin => \VGA_Ctrl_inst|Add4~34\,
	sumout => \VGA_Ctrl_inst|Add4~29_sumout\,
	cout => \VGA_Ctrl_inst|Add4~30\);

-- Location: FF_X37_Y80_N11
\VGA_Ctrl_inst|H_Cont[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(3));

-- Location: LABCELL_X37_Y80_N12
\VGA_Ctrl_inst|Add4~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~25_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(4) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~30\ ))
-- \VGA_Ctrl_inst|Add4~26\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(4) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_H_Cont\(4),
	cin => \VGA_Ctrl_inst|Add4~30\,
	sumout => \VGA_Ctrl_inst|Add4~25_sumout\,
	cout => \VGA_Ctrl_inst|Add4~26\);

-- Location: FF_X37_Y80_N14
\VGA_Ctrl_inst|H_Cont[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(4));

-- Location: LABCELL_X37_Y80_N15
\VGA_Ctrl_inst|Add4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~9_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(5) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~26\ ))
-- \VGA_Ctrl_inst|Add4~10\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(5) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(5),
	cin => \VGA_Ctrl_inst|Add4~26\,
	sumout => \VGA_Ctrl_inst|Add4~9_sumout\,
	cout => \VGA_Ctrl_inst|Add4~10\);

-- Location: FF_X37_Y80_N16
\VGA_Ctrl_inst|H_Cont[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(5));

-- Location: LABCELL_X37_Y80_N18
\VGA_Ctrl_inst|Add4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~5_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(6) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~10\ ))
-- \VGA_Ctrl_inst|Add4~6\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(6) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_H_Cont\(6),
	cin => \VGA_Ctrl_inst|Add4~10\,
	sumout => \VGA_Ctrl_inst|Add4~5_sumout\,
	cout => \VGA_Ctrl_inst|Add4~6\);

-- Location: FF_X37_Y80_N19
\VGA_Ctrl_inst|H_Cont[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(6));

-- Location: LABCELL_X37_Y80_N21
\VGA_Ctrl_inst|Add4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~1_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(7) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~6\ ))
-- \VGA_Ctrl_inst|Add4~2\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(7) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_H_Cont\(7),
	cin => \VGA_Ctrl_inst|Add4~6\,
	sumout => \VGA_Ctrl_inst|Add4~1_sumout\,
	cout => \VGA_Ctrl_inst|Add4~2\);

-- Location: FF_X37_Y80_N23
\VGA_Ctrl_inst|H_Cont[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(7));

-- Location: LABCELL_X37_Y80_N24
\VGA_Ctrl_inst|Add4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~13_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(8) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~2\ ))
-- \VGA_Ctrl_inst|Add4~14\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(8) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(8),
	cin => \VGA_Ctrl_inst|Add4~2\,
	sumout => \VGA_Ctrl_inst|Add4~13_sumout\,
	cout => \VGA_Ctrl_inst|Add4~14\);

-- Location: FF_X37_Y80_N26
\VGA_Ctrl_inst|H_Cont[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(8));

-- Location: LABCELL_X37_Y80_N27
\VGA_Ctrl_inst|Add4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~17_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(9) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~14\ ))
-- \VGA_Ctrl_inst|Add4~18\ = CARRY(( \VGA_Ctrl_inst|H_Cont\(9) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_H_Cont\(9),
	cin => \VGA_Ctrl_inst|Add4~14\,
	sumout => \VGA_Ctrl_inst|Add4~17_sumout\,
	cout => \VGA_Ctrl_inst|Add4~18\);

-- Location: FF_X37_Y80_N29
\VGA_Ctrl_inst|H_Cont[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(9));

-- Location: LABCELL_X37_Y80_N30
\VGA_Ctrl_inst|Add4~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add4~21_sumout\ = SUM(( \VGA_Ctrl_inst|H_Cont\(10) ) + ( GND ) + ( \VGA_Ctrl_inst|Add4~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_H_Cont\(10),
	cin => \VGA_Ctrl_inst|Add4~18\,
	sumout => \VGA_Ctrl_inst|Add4~21_sumout\);

-- Location: FF_X37_Y80_N32
\VGA_Ctrl_inst|H_Cont[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(10));

-- Location: LABCELL_X37_Y80_N48
\VGA_Ctrl_inst|LessThan2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|LessThan2~0_combout\ = ( \VGA_Ctrl_inst|H_Cont\(7) & ( \VGA_Ctrl_inst|H_Cont\(10) ) ) # ( !\VGA_Ctrl_inst|H_Cont\(7) & ( \VGA_Ctrl_inst|H_Cont\(10) ) ) # ( \VGA_Ctrl_inst|H_Cont\(7) & ( !\VGA_Ctrl_inst|H_Cont\(10) & ( 
-- (\VGA_Ctrl_inst|H_Cont\(9) & \VGA_Ctrl_inst|H_Cont\(8)) ) ) ) # ( !\VGA_Ctrl_inst|H_Cont\(7) & ( !\VGA_Ctrl_inst|H_Cont\(10) & ( (\VGA_Ctrl_inst|H_Cont\(9) & (\VGA_Ctrl_inst|H_Cont\(8) & ((\VGA_Ctrl_inst|H_Cont\(6)) # (\VGA_Ctrl_inst|H_Cont\(5))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000101000001010000010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_H_Cont\(9),
	datab => \VGA_Ctrl_inst|ALT_INV_H_Cont\(5),
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(8),
	datad => \VGA_Ctrl_inst|ALT_INV_H_Cont\(6),
	datae => \VGA_Ctrl_inst|ALT_INV_H_Cont\(7),
	dataf => \VGA_Ctrl_inst|ALT_INV_H_Cont\(10),
	combout => \VGA_Ctrl_inst|LessThan2~0_combout\);

-- Location: FF_X37_Y80_N2
\VGA_Ctrl_inst|H_Cont[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~41_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(0));

-- Location: FF_X37_Y80_N5
\VGA_Ctrl_inst|H_Cont[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~37_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(1));

-- Location: FF_X37_Y80_N8
\VGA_Ctrl_inst|H_Cont[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~inputCLKENA0_outclk\,
	d => \VGA_Ctrl_inst|Add4~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|H_Cont\(2));

-- Location: LABCELL_X37_Y80_N42
\VGA_Ctrl_inst|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Equal0~0_combout\ = ( \VGA_Ctrl_inst|H_Cont\(3) & ( (\VGA_Ctrl_inst|H_Cont\(1) & (\VGA_Ctrl_inst|H_Cont\(2) & (!\VGA_Ctrl_inst|H_Cont\(7) & \VGA_Ctrl_inst|H_Cont\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_H_Cont\(1),
	datab => \VGA_Ctrl_inst|ALT_INV_H_Cont\(2),
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(7),
	datad => \VGA_Ctrl_inst|ALT_INV_H_Cont\(0),
	dataf => \VGA_Ctrl_inst|ALT_INV_H_Cont\(3),
	combout => \VGA_Ctrl_inst|Equal0~0_combout\);

-- Location: LABCELL_X37_Y80_N45
\VGA_Ctrl_inst|oVGA_BLANK~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oVGA_BLANK~1_combout\ = ( !\VGA_Ctrl_inst|H_Cont\(8) & ( (!\VGA_Ctrl_inst|H_Cont\(10) & !\VGA_Ctrl_inst|H_Cont\(9)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(10),
	datad => \VGA_Ctrl_inst|ALT_INV_H_Cont\(9),
	dataf => \VGA_Ctrl_inst|ALT_INV_H_Cont\(8),
	combout => \VGA_Ctrl_inst|oVGA_BLANK~1_combout\);

-- Location: LABCELL_X37_Y80_N54
\VGA_Ctrl_inst|oVGA_HS~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oVGA_HS~0_combout\ = ( \VGA_Ctrl_inst|oVGA_BLANK~1_combout\ & ( \VGA_Ctrl_inst|H_Cont\(6) & ( (\VGA_Ctrl_inst|oVGA_HS~q\ & ((!\VGA_Ctrl_inst|Equal0~0_combout\) # ((!\VGA_Ctrl_inst|H_Cont\(5)) # (\VGA_Ctrl_inst|H_Cont\(4))))) ) ) ) # ( 
-- !\VGA_Ctrl_inst|oVGA_BLANK~1_combout\ & ( \VGA_Ctrl_inst|H_Cont\(6) & ( \VGA_Ctrl_inst|oVGA_HS~q\ ) ) ) # ( \VGA_Ctrl_inst|oVGA_BLANK~1_combout\ & ( !\VGA_Ctrl_inst|H_Cont\(6) & ( ((\VGA_Ctrl_inst|Equal0~0_combout\ & (!\VGA_Ctrl_inst|H_Cont\(4) & 
-- !\VGA_Ctrl_inst|H_Cont\(5)))) # (\VGA_Ctrl_inst|oVGA_HS~q\) ) ) ) # ( !\VGA_Ctrl_inst|oVGA_BLANK~1_combout\ & ( !\VGA_Ctrl_inst|H_Cont\(6) & ( \VGA_Ctrl_inst|oVGA_HS~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010011110000111100001111000011110000111100001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_Equal0~0_combout\,
	datab => \VGA_Ctrl_inst|ALT_INV_H_Cont\(4),
	datac => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	datad => \VGA_Ctrl_inst|ALT_INV_H_Cont\(5),
	datae => \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~1_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_H_Cont\(6),
	combout => \VGA_Ctrl_inst|oVGA_HS~0_combout\);

-- Location: FF_X39_Y80_N38
\VGA_Ctrl_inst|oVGA_HS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TD_CLK27~input_o\,
	asdata => \VGA_Ctrl_inst|oVGA_HS~0_combout\,
	clrn => \KEY[3]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|oVGA_HS~q\);

-- Location: MLABCELL_X39_Y80_N0
\VGA_Ctrl_inst|Add5~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~37_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(0) ) + ( VCC ) + ( !VCC ))
-- \VGA_Ctrl_inst|Add5~38\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(0),
	cin => GND,
	sumout => \VGA_Ctrl_inst|Add5~37_sumout\,
	cout => \VGA_Ctrl_inst|Add5~38\);

-- Location: MLABCELL_X39_Y80_N15
\VGA_Ctrl_inst|Add5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~1_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(5) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~6\ ))
-- \VGA_Ctrl_inst|Add5~2\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(5) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	cin => \VGA_Ctrl_inst|Add5~6\,
	sumout => \VGA_Ctrl_inst|Add5~1_sumout\,
	cout => \VGA_Ctrl_inst|Add5~2\);

-- Location: MLABCELL_X39_Y80_N18
\VGA_Ctrl_inst|Add5~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~25_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(6) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~2\ ))
-- \VGA_Ctrl_inst|Add5~26\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(6) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(6),
	cin => \VGA_Ctrl_inst|Add5~2\,
	sumout => \VGA_Ctrl_inst|Add5~25_sumout\,
	cout => \VGA_Ctrl_inst|Add5~26\);

-- Location: FF_X39_Y80_N20
\VGA_Ctrl_inst|V_Cont[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(6));

-- Location: MLABCELL_X39_Y80_N21
\VGA_Ctrl_inst|Add5~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~29_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(7) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~26\ ))
-- \VGA_Ctrl_inst|Add5~30\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(7) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(7),
	cin => \VGA_Ctrl_inst|Add5~26\,
	sumout => \VGA_Ctrl_inst|Add5~29_sumout\,
	cout => \VGA_Ctrl_inst|Add5~30\);

-- Location: FF_X39_Y80_N22
\VGA_Ctrl_inst|V_Cont[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(7));

-- Location: MLABCELL_X39_Y80_N24
\VGA_Ctrl_inst|Add5~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~33_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(8) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~30\ ))
-- \VGA_Ctrl_inst|Add5~34\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(8) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(8),
	cin => \VGA_Ctrl_inst|Add5~30\,
	sumout => \VGA_Ctrl_inst|Add5~33_sumout\,
	cout => \VGA_Ctrl_inst|Add5~34\);

-- Location: FF_X39_Y80_N25
\VGA_Ctrl_inst|V_Cont[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(8));

-- Location: MLABCELL_X39_Y80_N27
\VGA_Ctrl_inst|Add5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~17_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(9) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~34\ ))
-- \VGA_Ctrl_inst|Add5~18\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(9) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(9),
	cin => \VGA_Ctrl_inst|Add5~34\,
	sumout => \VGA_Ctrl_inst|Add5~17_sumout\,
	cout => \VGA_Ctrl_inst|Add5~18\);

-- Location: FF_X39_Y80_N29
\VGA_Ctrl_inst|V_Cont[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(9));

-- Location: MLABCELL_X39_Y80_N30
\VGA_Ctrl_inst|Add5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~21_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(10) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(10),
	cin => \VGA_Ctrl_inst|Add5~18\,
	sumout => \VGA_Ctrl_inst|Add5~21_sumout\);

-- Location: FF_X39_Y80_N31
\VGA_Ctrl_inst|V_Cont[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(10));

-- Location: MLABCELL_X39_Y80_N45
\VGA_Ctrl_inst|LessThan1~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|LessThan1~3_combout\ = ( !\VGA_Ctrl_inst|V_Cont\(7) & ( (!\VGA_Ctrl_inst|V_Cont\(6) & !\VGA_Ctrl_inst|V_Cont\(8)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(6),
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(8),
	dataf => \VGA_Ctrl_inst|ALT_INV_V_Cont\(7),
	combout => \VGA_Ctrl_inst|LessThan1~3_combout\);

-- Location: MLABCELL_X39_Y80_N39
\VGA_Ctrl_inst|LessThan1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|LessThan1~0_combout\ = ( !\VGA_Ctrl_inst|V_Cont\(2) & ( \VGA_Ctrl_inst|V_Cont\(3) & ( !\VGA_Ctrl_inst|V_Cont\(4) ) ) ) # ( \VGA_Ctrl_inst|V_Cont\(2) & ( !\VGA_Ctrl_inst|V_Cont\(3) & ( !\VGA_Ctrl_inst|V_Cont\(4) ) ) ) # ( 
-- !\VGA_Ctrl_inst|V_Cont\(2) & ( !\VGA_Ctrl_inst|V_Cont\(3) & ( !\VGA_Ctrl_inst|V_Cont\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(4),
	datae => \VGA_Ctrl_inst|ALT_INV_V_Cont\(2),
	dataf => \VGA_Ctrl_inst|ALT_INV_V_Cont\(3),
	combout => \VGA_Ctrl_inst|LessThan1~0_combout\);

-- Location: MLABCELL_X39_Y80_N54
\VGA_Ctrl_inst|LessThan3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|LessThan3~0_combout\ = ( \VGA_Ctrl_inst|LessThan1~0_combout\ & ( ((\VGA_Ctrl_inst|V_Cont\(9) & ((!\VGA_Ctrl_inst|LessThan1~3_combout\) # (\VGA_Ctrl_inst|V_Cont\(5))))) # (\VGA_Ctrl_inst|V_Cont\(10)) ) ) # ( 
-- !\VGA_Ctrl_inst|LessThan1~0_combout\ & ( (\VGA_Ctrl_inst|V_Cont\(10)) # (\VGA_Ctrl_inst|V_Cont\(9)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111101011111000111110101111100011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(9),
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(10),
	datad => \VGA_Ctrl_inst|ALT_INV_LessThan1~3_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	combout => \VGA_Ctrl_inst|LessThan3~0_combout\);

-- Location: FF_X39_Y80_N2
\VGA_Ctrl_inst|V_Cont[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~37_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(0));

-- Location: MLABCELL_X39_Y80_N3
\VGA_Ctrl_inst|Add5~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~41_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(1) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~38\ ))
-- \VGA_Ctrl_inst|Add5~42\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(1) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(1),
	cin => \VGA_Ctrl_inst|Add5~38\,
	sumout => \VGA_Ctrl_inst|Add5~41_sumout\,
	cout => \VGA_Ctrl_inst|Add5~42\);

-- Location: FF_X39_Y80_N5
\VGA_Ctrl_inst|V_Cont[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~41_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(1));

-- Location: MLABCELL_X39_Y80_N6
\VGA_Ctrl_inst|Add5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~9_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(2) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~42\ ))
-- \VGA_Ctrl_inst|Add5~10\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(2) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(2),
	cin => \VGA_Ctrl_inst|Add5~42\,
	sumout => \VGA_Ctrl_inst|Add5~9_sumout\,
	cout => \VGA_Ctrl_inst|Add5~10\);

-- Location: FF_X39_Y80_N7
\VGA_Ctrl_inst|V_Cont[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(2));

-- Location: MLABCELL_X39_Y80_N9
\VGA_Ctrl_inst|Add5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~13_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(3) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~10\ ))
-- \VGA_Ctrl_inst|Add5~14\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(3) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(3),
	cin => \VGA_Ctrl_inst|Add5~10\,
	sumout => \VGA_Ctrl_inst|Add5~13_sumout\,
	cout => \VGA_Ctrl_inst|Add5~14\);

-- Location: FF_X39_Y80_N11
\VGA_Ctrl_inst|V_Cont[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(3));

-- Location: MLABCELL_X39_Y80_N12
\VGA_Ctrl_inst|Add5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add5~5_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(4) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~14\ ))
-- \VGA_Ctrl_inst|Add5~6\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(4) ) + ( GND ) + ( \VGA_Ctrl_inst|Add5~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(4),
	cin => \VGA_Ctrl_inst|Add5~14\,
	sumout => \VGA_Ctrl_inst|Add5~5_sumout\,
	cout => \VGA_Ctrl_inst|Add5~6\);

-- Location: FF_X39_Y80_N13
\VGA_Ctrl_inst|V_Cont[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(4));

-- Location: FF_X39_Y80_N16
\VGA_Ctrl_inst|V_Cont[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont\(5));

-- Location: LABCELL_X37_Y80_N36
\VGA_Ctrl_inst|oVGA_BLANK~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oVGA_BLANK~0_combout\ = ( \VGA_Ctrl_inst|H_Cont\(6) & ( \VGA_Ctrl_inst|H_Cont\(7) ) ) # ( !\VGA_Ctrl_inst|H_Cont\(6) & ( (\VGA_Ctrl_inst|H_Cont\(7) & \VGA_Ctrl_inst|H_Cont\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_H_Cont\(7),
	datad => \VGA_Ctrl_inst|ALT_INV_H_Cont\(5),
	dataf => \VGA_Ctrl_inst|ALT_INV_H_Cont\(6),
	combout => \VGA_Ctrl_inst|oVGA_BLANK~0_combout\);

-- Location: MLABCELL_X39_Y80_N42
\VGA_Ctrl_inst|LessThan1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|LessThan1~1_combout\ = ( !\VGA_Ctrl_inst|V_Cont\(7) & ( (!\VGA_Ctrl_inst|V_Cont\(6) & (!\VGA_Ctrl_inst|V_Cont\(8) & (!\VGA_Ctrl_inst|V_Cont\(10) & !\VGA_Ctrl_inst|V_Cont\(9)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(6),
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(8),
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(10),
	datad => \VGA_Ctrl_inst|ALT_INV_V_Cont\(9),
	dataf => \VGA_Ctrl_inst|ALT_INV_V_Cont\(7),
	combout => \VGA_Ctrl_inst|LessThan1~1_combout\);

-- Location: LABCELL_X37_Y80_N39
\VGA_Ctrl_inst|oVGA_BLANK~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oVGA_BLANK~2_combout\ = ( \VGA_Ctrl_inst|LessThan1~1_combout\ & ( (!\VGA_Ctrl_inst|V_Cont\(5)) # (((\VGA_Ctrl_inst|oVGA_BLANK~1_combout\ & !\VGA_Ctrl_inst|oVGA_BLANK~0_combout\)) # (\VGA_Ctrl_inst|LessThan1~0_combout\)) ) ) # ( 
-- !\VGA_Ctrl_inst|LessThan1~1_combout\ & ( (\VGA_Ctrl_inst|oVGA_BLANK~1_combout\ & !\VGA_Ctrl_inst|oVGA_BLANK~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000010111111101011111011111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datab => \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~1_combout\,
	datac => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	datad => \VGA_Ctrl_inst|ALT_INV_oVGA_BLANK~0_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	combout => \VGA_Ctrl_inst|oVGA_BLANK~2_combout\);

-- Location: FF_X39_Y80_N10
\VGA_Ctrl_inst|V_Cont[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont[3]~DUPLICATE_q\);

-- Location: LABCELL_X40_Y80_N0
\VGA_Ctrl_inst|Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~21_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(2) ) + ( VCC ) + ( !VCC ))
-- \VGA_Ctrl_inst|Add3~22\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(2) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(2),
	cin => GND,
	sumout => \VGA_Ctrl_inst|Add3~21_sumout\,
	cout => \VGA_Ctrl_inst|Add3~22\);

-- Location: LABCELL_X40_Y80_N3
\VGA_Ctrl_inst|Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~17_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont[3]~DUPLICATE_q\ ) + ( GND ) + ( \VGA_Ctrl_inst|Add3~22\ ))
-- \VGA_Ctrl_inst|Add3~18\ = CARRY(( \VGA_Ctrl_inst|V_Cont[3]~DUPLICATE_q\ ) + ( GND ) + ( \VGA_Ctrl_inst|Add3~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont[3]~DUPLICATE_q\,
	cin => \VGA_Ctrl_inst|Add3~22\,
	sumout => \VGA_Ctrl_inst|Add3~17_sumout\,
	cout => \VGA_Ctrl_inst|Add3~18\);

-- Location: LABCELL_X40_Y80_N6
\VGA_Ctrl_inst|Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~25_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(4) ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~18\ ))
-- \VGA_Ctrl_inst|Add3~26\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(4) ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(4),
	cin => \VGA_Ctrl_inst|Add3~18\,
	sumout => \VGA_Ctrl_inst|Add3~25_sumout\,
	cout => \VGA_Ctrl_inst|Add3~26\);

-- Location: LABCELL_X40_Y80_N9
\VGA_Ctrl_inst|Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~1_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(5) ) + ( GND ) + ( \VGA_Ctrl_inst|Add3~26\ ))
-- \VGA_Ctrl_inst|Add3~2\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(5) ) + ( GND ) + ( \VGA_Ctrl_inst|Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	cin => \VGA_Ctrl_inst|Add3~26\,
	sumout => \VGA_Ctrl_inst|Add3~1_sumout\,
	cout => \VGA_Ctrl_inst|Add3~2\);

-- Location: LABCELL_X40_Y80_N33
\VGA_Ctrl_inst|oCurrent_Y[5]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\ = ( \VGA_Ctrl_inst|Add3~1_sumout\ & ( (!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((!\VGA_Ctrl_inst|LessThan1~0_combout\ & \VGA_Ctrl_inst|V_Cont\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110010111100101111001011110010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datac => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_Add3~1_sumout\,
	combout => \VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\);

-- Location: LABCELL_X40_Y80_N27
\VGA_Ctrl_inst|LessThan1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|LessThan1~2_combout\ = ( \VGA_Ctrl_inst|LessThan1~0_combout\ & ( \VGA_Ctrl_inst|LessThan1~1_combout\ ) ) # ( !\VGA_Ctrl_inst|LessThan1~0_combout\ & ( (\VGA_Ctrl_inst|LessThan1~1_combout\ & !\VGA_Ctrl_inst|V_Cont\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	dataf => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	combout => \VGA_Ctrl_inst|LessThan1~2_combout\);

-- Location: FF_X39_Y80_N4
\VGA_Ctrl_inst|V_Cont[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~41_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont[1]~DUPLICATE_q\);

-- Location: MLABCELL_X39_Y80_N51
\VGA_Ctrl_inst|oCurrent_Y[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oCurrent_Y[0]~1_combout\ = ( \VGA_Ctrl_inst|V_Cont\(5) & ( (\VGA_Ctrl_inst|V_Cont\(0) & ((!\VGA_Ctrl_inst|LessThan1~1_combout\) # (!\VGA_Ctrl_inst|LessThan1~0_combout\))) ) ) # ( !\VGA_Ctrl_inst|V_Cont\(5) & ( 
-- (!\VGA_Ctrl_inst|LessThan1~1_combout\ & \VGA_Ctrl_inst|V_Cont\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000001100110000000000111111000000000011111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	datac => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	datad => \VGA_Ctrl_inst|ALT_INV_V_Cont\(0),
	dataf => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	combout => \VGA_Ctrl_inst|oCurrent_Y[0]~1_combout\);

-- Location: FF_X39_Y80_N19
\VGA_Ctrl_inst|V_Cont[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	d => \VGA_Ctrl_inst|Add5~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \VGA_Ctrl_inst|LessThan3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|V_Cont[6]~DUPLICATE_q\);

-- Location: LABCELL_X40_Y80_N12
\VGA_Ctrl_inst|Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~9_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont[6]~DUPLICATE_q\ ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~2\ ))
-- \VGA_Ctrl_inst|Add3~10\ = CARRY(( \VGA_Ctrl_inst|V_Cont[6]~DUPLICATE_q\ ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont[6]~DUPLICATE_q\,
	cin => \VGA_Ctrl_inst|Add3~2\,
	sumout => \VGA_Ctrl_inst|Add3~9_sumout\,
	cout => \VGA_Ctrl_inst|Add3~10\);

-- Location: LABCELL_X40_Y80_N36
\display_inst|Add0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \display_inst|Add0~0_combout\ = ( \VGA_Ctrl_inst|Add3~9_sumout\ & ( (!\VGA_Ctrl_inst|Add3~1_sumout\ & ((!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((!\VGA_Ctrl_inst|LessThan1~0_combout\ & \VGA_Ctrl_inst|V_Cont\(5))))) ) ) # ( !\VGA_Ctrl_inst|Add3~9_sumout\ & 
-- ( (\VGA_Ctrl_inst|Add3~1_sumout\ & ((!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((!\VGA_Ctrl_inst|LessThan1~0_combout\ & \VGA_Ctrl_inst|V_Cont\(5))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000010000011110000001011110000001000001111000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datac => \VGA_Ctrl_inst|ALT_INV_Add3~1_sumout\,
	datad => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_Add3~9_sumout\,
	combout => \display_inst|Add0~0_combout\);

-- Location: LABCELL_X40_Y80_N39
\VGA_Ctrl_inst|oCurrent_Y[4]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oCurrent_Y[4]~2_combout\ = ( \VGA_Ctrl_inst|Add3~25_sumout\ & ( (!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((!\VGA_Ctrl_inst|LessThan1~0_combout\ & \VGA_Ctrl_inst|V_Cont\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000010101111111100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datad => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_Add3~25_sumout\,
	combout => \VGA_Ctrl_inst|oCurrent_Y[4]~2_combout\);

-- Location: LABCELL_X40_Y80_N42
\display_inst|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \display_inst|Mux0~0_combout\ = ( \display_inst|Add0~0_combout\ & ( \VGA_Ctrl_inst|oCurrent_Y[4]~2_combout\ & ( (\VGA_Ctrl_inst|V_Cont[1]~DUPLICATE_q\ & \VGA_Ctrl_inst|oCurrent_Y[0]~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont[1]~DUPLICATE_q\,
	datac => \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[0]~1_combout\,
	datae => \display_inst|ALT_INV_Add0~0_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[4]~2_combout\,
	combout => \display_inst|Mux0~0_combout\);

-- Location: LABCELL_X40_Y80_N48
\display_inst|Mux0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \display_inst|Mux0~1_combout\ = ( \display_inst|Mux0~0_combout\ & ( (\VGA_Ctrl_inst|Add3~21_sumout\ & (\VGA_Ctrl_inst|Add3~17_sumout\ & !\VGA_Ctrl_inst|LessThan1~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101000000000000010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_Add3~21_sumout\,
	datac => \VGA_Ctrl_inst|ALT_INV_Add3~17_sumout\,
	datad => \VGA_Ctrl_inst|ALT_INV_LessThan1~2_combout\,
	dataf => \display_inst|ALT_INV_Mux0~0_combout\,
	combout => \display_inst|Mux0~1_combout\);

-- Location: LABCELL_X40_Y80_N15
\VGA_Ctrl_inst|Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~5_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(7) ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~10\ ))
-- \VGA_Ctrl_inst|Add3~6\ = CARRY(( \VGA_Ctrl_inst|V_Cont\(7) ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(7),
	cin => \VGA_Ctrl_inst|Add3~10\,
	sumout => \VGA_Ctrl_inst|Add3~5_sumout\,
	cout => \VGA_Ctrl_inst|Add3~6\);

-- Location: LABCELL_X40_Y80_N51
\VGA_Ctrl_inst|oCurrent_Y[7]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\ = ( \VGA_Ctrl_inst|Add3~5_sumout\ & ( (!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((\VGA_Ctrl_inst|V_Cont\(5) & !\VGA_Ctrl_inst|LessThan1~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011001111110011001100111111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datad => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_Add3~5_sumout\,
	combout => \VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\);

-- Location: LABCELL_X40_Y80_N30
\VGA_Ctrl_inst|oCurrent_Y[6]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oCurrent_Y[6]~0_combout\ = ( \VGA_Ctrl_inst|Add3~9_sumout\ & ( (!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((!\VGA_Ctrl_inst|LessThan1~0_combout\ & \VGA_Ctrl_inst|V_Cont\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111001000101111111100100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_LessThan1~0_combout\,
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datad => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_Add3~9_sumout\,
	combout => \VGA_Ctrl_inst|oCurrent_Y[6]~0_combout\);

-- Location: LABCELL_X40_Y80_N24
\display_inst|Mux0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \display_inst|Mux0~2_combout\ = ( \VGA_Ctrl_inst|oCurrent_Y[6]~0_combout\ & ( (!\VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\ & (!\display_inst|Mux0~1_combout\ & !\VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\)) # (\VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\ & 
-- ((\VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\))) ) ) # ( !\VGA_Ctrl_inst|oCurrent_Y[6]~0_combout\ & ( (!\VGA_Ctrl_inst|oCurrent_Y[7]~4_combout\ & ((!\display_inst|Mux0~1_combout\) # (\VGA_Ctrl_inst|oCurrent_Y[5]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010100000000111101010000000010100000010101011010000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[5]~3_combout\,
	datac => \display_inst|ALT_INV_Mux0~1_combout\,
	datad => \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[7]~4_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_oCurrent_Y[6]~0_combout\,
	combout => \display_inst|Mux0~2_combout\);

-- Location: LABCELL_X40_Y80_N18
\VGA_Ctrl_inst|Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Add3~13_sumout\ = SUM(( \VGA_Ctrl_inst|V_Cont\(8) ) + ( VCC ) + ( \VGA_Ctrl_inst|Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(8),
	cin => \VGA_Ctrl_inst|Add3~6\,
	sumout => \VGA_Ctrl_inst|Add3~13_sumout\);

-- Location: LABCELL_X40_Y80_N54
\display_inst|Mux0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \display_inst|Mux0~3_combout\ = ( !\VGA_Ctrl_inst|LessThan1~2_combout\ & ( \VGA_Ctrl_inst|Add3~1_sumout\ & ( (!\display_inst|Mux0~2_combout\ & (!\VGA_Ctrl_inst|Add3~13_sumout\ $ (((!\VGA_Ctrl_inst|Add3~9_sumout\) # (!\VGA_Ctrl_inst|Add3~5_sumout\))))) ) ) 
-- ) # ( !\VGA_Ctrl_inst|LessThan1~2_combout\ & ( !\VGA_Ctrl_inst|Add3~1_sumout\ & ( (!\display_inst|Mux0~2_combout\ & \VGA_Ctrl_inst|Add3~13_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000000000000000000001010001010000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \display_inst|ALT_INV_Mux0~2_combout\,
	datab => \VGA_Ctrl_inst|ALT_INV_Add3~9_sumout\,
	datac => \VGA_Ctrl_inst|ALT_INV_Add3~13_sumout\,
	datad => \VGA_Ctrl_inst|ALT_INV_Add3~5_sumout\,
	datae => \VGA_Ctrl_inst|ALT_INV_LessThan1~2_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_Add3~1_sumout\,
	combout => \display_inst|Mux0~3_combout\);

-- Location: MLABCELL_X39_Y80_N57
\VGA_Ctrl_inst|Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|Equal2~0_combout\ = ( \VGA_Ctrl_inst|V_Cont\(3) & ( (!\VGA_Ctrl_inst|V_Cont\(5) & (!\VGA_Ctrl_inst|V_Cont\(4) & !\VGA_Ctrl_inst|V_Cont\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \VGA_Ctrl_inst|ALT_INV_V_Cont\(5),
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(4),
	datad => \VGA_Ctrl_inst|ALT_INV_V_Cont\(0),
	dataf => \VGA_Ctrl_inst|ALT_INV_V_Cont\(3),
	combout => \VGA_Ctrl_inst|Equal2~0_combout\);

-- Location: MLABCELL_X39_Y80_N48
\VGA_Ctrl_inst|oVGA_VS~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \VGA_Ctrl_inst|oVGA_VS~0_combout\ = ( \VGA_Ctrl_inst|oVGA_VS~q\ & ( ((!\VGA_Ctrl_inst|LessThan1~1_combout\) # ((!\VGA_Ctrl_inst|V_Cont\(2)) # (!\VGA_Ctrl_inst|Equal2~0_combout\))) # (\VGA_Ctrl_inst|V_Cont\(1)) ) ) # ( !\VGA_Ctrl_inst|oVGA_VS~q\ & ( 
-- (\VGA_Ctrl_inst|V_Cont\(1) & (\VGA_Ctrl_inst|LessThan1~1_combout\ & (!\VGA_Ctrl_inst|V_Cont\(2) & \VGA_Ctrl_inst|Equal2~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000001000011111111111111011111111111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \VGA_Ctrl_inst|ALT_INV_V_Cont\(1),
	datab => \VGA_Ctrl_inst|ALT_INV_LessThan1~1_combout\,
	datac => \VGA_Ctrl_inst|ALT_INV_V_Cont\(2),
	datad => \VGA_Ctrl_inst|ALT_INV_Equal2~0_combout\,
	dataf => \VGA_Ctrl_inst|ALT_INV_oVGA_VS~q\,
	combout => \VGA_Ctrl_inst|oVGA_VS~0_combout\);

-- Location: FF_X39_Y80_N35
\VGA_Ctrl_inst|oVGA_VS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \VGA_Ctrl_inst|ALT_INV_oVGA_HS~q\,
	asdata => \VGA_Ctrl_inst|oVGA_VS~0_combout\,
	clrn => \KEY[3]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \VGA_Ctrl_inst|oVGA_VS~q\);

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: IOIBUF_X12_Y0_N18
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: IOIBUF_X16_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X8_Y0_N35
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X4_Y0_N52
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X2_Y0_N41
\SW[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X16_Y0_N18
\SW[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: IOIBUF_X4_Y0_N35
\SW[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: IOIBUF_X4_Y0_N1
\SW[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: IOIBUF_X4_Y0_N18
\SW[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X2_Y0_N58
\SW[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: IOIBUF_X64_Y0_N1
\GPIO_0[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(0),
	o => \GPIO_0[0]~input_o\);

-- Location: IOIBUF_X68_Y0_N1
\GPIO_0[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(1),
	o => \GPIO_0[1]~input_o\);

-- Location: IOIBUF_X64_Y0_N18
\GPIO_0[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(2),
	o => \GPIO_0[2]~input_o\);

-- Location: IOIBUF_X72_Y0_N1
\GPIO_0[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(3),
	o => \GPIO_0[3]~input_o\);

-- Location: IOIBUF_X54_Y0_N52
\GPIO_0[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(4),
	o => \GPIO_0[4]~input_o\);

-- Location: IOIBUF_X58_Y0_N58
\GPIO_0[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(5),
	o => \GPIO_0[5]~input_o\);

-- Location: IOIBUF_X60_Y0_N52
\GPIO_0[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(6),
	o => \GPIO_0[6]~input_o\);

-- Location: IOIBUF_X60_Y0_N35
\GPIO_0[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(7),
	o => \GPIO_0[7]~input_o\);

-- Location: IOIBUF_X58_Y0_N41
\GPIO_0[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(8),
	o => \GPIO_0[8]~input_o\);

-- Location: IOIBUF_X54_Y0_N35
\GPIO_0[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(9),
	o => \GPIO_0[9]~input_o\);

-- Location: IOIBUF_X56_Y0_N52
\GPIO_0[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(10),
	o => \GPIO_0[10]~input_o\);

-- Location: IOIBUF_X56_Y0_N35
\GPIO_0[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(11),
	o => \GPIO_0[11]~input_o\);

-- Location: IOIBUF_X50_Y0_N75
\GPIO_0[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(12),
	o => \GPIO_0[12]~input_o\);

-- Location: IOIBUF_X52_Y0_N35
\GPIO_0[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(13),
	o => \GPIO_0[13]~input_o\);

-- Location: IOIBUF_X52_Y0_N52
\GPIO_0[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(14),
	o => \GPIO_0[14]~input_o\);

-- Location: IOIBUF_X50_Y0_N92
\GPIO_0[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(15),
	o => \GPIO_0[15]~input_o\);

-- Location: IOIBUF_X68_Y0_N18
\GPIO_0[16]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(16),
	o => \GPIO_0[16]~input_o\);

-- Location: IOIBUF_X72_Y0_N18
\GPIO_0[17]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(17),
	o => \GPIO_0[17]~input_o\);

-- Location: IOIBUF_X50_Y0_N41
\GPIO_0[18]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(18),
	o => \GPIO_0[18]~input_o\);

-- Location: IOIBUF_X76_Y0_N1
\GPIO_0[19]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(19),
	o => \GPIO_0[19]~input_o\);

-- Location: IOIBUF_X58_Y0_N92
\GPIO_0[20]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(20),
	o => \GPIO_0[20]~input_o\);

-- Location: IOIBUF_X62_Y0_N35
\GPIO_0[21]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(21),
	o => \GPIO_0[21]~input_o\);

-- Location: IOIBUF_X54_Y0_N18
\GPIO_0[22]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(22),
	o => \GPIO_0[22]~input_o\);

-- Location: IOIBUF_X68_Y0_N35
\GPIO_0[23]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(23),
	o => \GPIO_0[23]~input_o\);

-- Location: IOIBUF_X76_Y0_N18
\GPIO_0[24]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(24),
	o => \GPIO_0[24]~input_o\);

-- Location: IOIBUF_X82_Y0_N41
\GPIO_0[25]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(25),
	o => \GPIO_0[25]~input_o\);

-- Location: IOIBUF_X66_Y0_N41
\GPIO_0[26]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(26),
	o => \GPIO_0[26]~input_o\);

-- Location: IOIBUF_X66_Y0_N58
\GPIO_0[27]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(27),
	o => \GPIO_0[27]~input_o\);

-- Location: IOIBUF_X70_Y0_N1
\GPIO_0[28]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(28),
	o => \GPIO_0[28]~input_o\);

-- Location: IOIBUF_X70_Y0_N18
\GPIO_0[29]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(29),
	o => \GPIO_0[29]~input_o\);

-- Location: IOIBUF_X62_Y0_N1
\GPIO_0[30]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(30),
	o => \GPIO_0[30]~input_o\);

-- Location: IOIBUF_X54_Y0_N1
\GPIO_0[31]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(31),
	o => \GPIO_0[31]~input_o\);

-- Location: IOIBUF_X50_Y0_N58
\GPIO_0[32]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(32),
	o => \GPIO_0[32]~input_o\);

-- Location: IOIBUF_X62_Y0_N18
\GPIO_0[33]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(33),
	o => \GPIO_0[33]~input_o\);

-- Location: IOIBUF_X58_Y0_N75
\GPIO_0[34]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(34),
	o => \GPIO_0[34]~input_o\);

-- Location: IOIBUF_X62_Y0_N52
\GPIO_0[35]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_0(35),
	o => \GPIO_0[35]~input_o\);

-- Location: IOIBUF_X56_Y0_N18
\GPIO_1[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(0),
	o => \GPIO_1[0]~input_o\);

-- Location: IOIBUF_X88_Y0_N2
\GPIO_1[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(1),
	o => \GPIO_1[1]~input_o\);

-- Location: IOIBUF_X88_Y0_N19
\GPIO_1[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(2),
	o => \GPIO_1[2]~input_o\);

-- Location: IOIBUF_X86_Y0_N18
\GPIO_1[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(3),
	o => \GPIO_1[3]~input_o\);

-- Location: IOIBUF_X88_Y0_N36
\GPIO_1[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(4),
	o => \GPIO_1[4]~input_o\);

-- Location: IOIBUF_X78_Y0_N18
\GPIO_1[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(5),
	o => \GPIO_1[5]~input_o\);

-- Location: IOIBUF_X88_Y0_N53
\GPIO_1[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(6),
	o => \GPIO_1[6]~input_o\);

-- Location: IOIBUF_X86_Y0_N35
\GPIO_1[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(7),
	o => \GPIO_1[7]~input_o\);

-- Location: IOIBUF_X86_Y0_N52
\GPIO_1[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(8),
	o => \GPIO_1[8]~input_o\);

-- Location: IOIBUF_X78_Y0_N35
\GPIO_1[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(9),
	o => \GPIO_1[9]~input_o\);

-- Location: IOIBUF_X84_Y0_N35
\GPIO_1[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(10),
	o => \GPIO_1[10]~input_o\);

-- Location: IOIBUF_X64_Y0_N52
\GPIO_1[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(11),
	o => \GPIO_1[11]~input_o\);

-- Location: IOIBUF_X84_Y0_N52
\GPIO_1[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(12),
	o => \GPIO_1[12]~input_o\);

-- Location: IOIBUF_X80_Y0_N35
\GPIO_1[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(13),
	o => \GPIO_1[13]~input_o\);

-- Location: IOIBUF_X82_Y0_N92
\GPIO_1[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(14),
	o => \GPIO_1[14]~input_o\);

-- Location: IOIBUF_X82_Y0_N75
\GPIO_1[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(15),
	o => \GPIO_1[15]~input_o\);

-- Location: IOIBUF_X80_Y0_N52
\GPIO_1[16]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(16),
	o => \GPIO_1[16]~input_o\);

-- Location: IOIBUF_X76_Y0_N35
\GPIO_1[17]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(17),
	o => \GPIO_1[17]~input_o\);

-- Location: IOIBUF_X76_Y0_N52
\GPIO_1[18]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(18),
	o => \GPIO_1[18]~input_o\);

-- Location: IOIBUF_X78_Y0_N52
\GPIO_1[19]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(19),
	o => \GPIO_1[19]~input_o\);

-- Location: IOIBUF_X74_Y0_N92
\GPIO_1[20]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(20),
	o => \GPIO_1[20]~input_o\);

-- Location: IOIBUF_X74_Y0_N75
\GPIO_1[21]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(21),
	o => \GPIO_1[21]~input_o\);

-- Location: IOIBUF_X72_Y0_N52
\GPIO_1[22]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(22),
	o => \GPIO_1[22]~input_o\);

-- Location: IOIBUF_X64_Y0_N35
\GPIO_1[23]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(23),
	o => \GPIO_1[23]~input_o\);

-- Location: IOIBUF_X72_Y0_N35
\GPIO_1[24]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(24),
	o => \GPIO_1[24]~input_o\);

-- Location: IOIBUF_X70_Y0_N35
\GPIO_1[25]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(25),
	o => \GPIO_1[25]~input_o\);

-- Location: IOIBUF_X68_Y0_N52
\GPIO_1[26]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(26),
	o => \GPIO_1[26]~input_o\);

-- Location: IOIBUF_X70_Y0_N52
\GPIO_1[27]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(27),
	o => \GPIO_1[27]~input_o\);

-- Location: IOIBUF_X66_Y0_N92
\GPIO_1[28]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(28),
	o => \GPIO_1[28]~input_o\);

-- Location: IOIBUF_X66_Y0_N75
\GPIO_1[29]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(29),
	o => \GPIO_1[29]~input_o\);

-- Location: IOIBUF_X74_Y0_N58
\GPIO_1[30]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(30),
	o => \GPIO_1[30]~input_o\);

-- Location: IOIBUF_X74_Y0_N41
\GPIO_1[31]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(31),
	o => \GPIO_1[31]~input_o\);

-- Location: IOIBUF_X78_Y0_N1
\GPIO_1[32]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(32),
	o => \GPIO_1[32]~input_o\);

-- Location: IOIBUF_X82_Y0_N58
\GPIO_1[33]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(33),
	o => \GPIO_1[33]~input_o\);

-- Location: IOIBUF_X84_Y0_N18
\GPIO_1[34]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(34),
	o => \GPIO_1[34]~input_o\);

-- Location: IOIBUF_X86_Y0_N1
\GPIO_1[35]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GPIO_1(35),
	o => \GPIO_1[35]~input_o\);

-- Location: LABCELL_X12_Y16_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


