-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Composant    : alu
-- Description  : Unité arithmétique et logique capable d'effectuer
--                8 opérations.
-- Auteur       : Yann Thoma
-- Date         : 13.02.2015
-- Version      : 1.0
--
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
generic (
	SIZE  : integer := 8
);
port (
	a_i      : in  std_logic_vector(SIZE-1 downto 0);
	b_i      : in  std_logic_vector(SIZE-1 downto 0);
	result_o : out std_logic_vector(SIZE-1 downto 0);
	carry_i  : in std_logic;
	carry_o  : out std_logic;
	mode_i   : in  std_logic_vector(2 downto 0)
);
end alu;

architecture behave of alu is
	signal comp_s			 	: std_logic;
	signal a_extended_s, b_extended_s	: std_logic_vector(SIZE downto 0);
	signal res_add_s, res_sub_s, carry_s	: std_logic_vector(SIZE downto 0);

begin
	comp_s 	<= '1' when a_i = b_i else '0';


	a_extended_s	<= '0' & a_i;
	b_extended_s	<= '0' & b_i;
	carry_s		<= "00000000" & carry_i;

	res_add_s	<= std_logic_vector(unsigned(a_extended_s) + unsigned(b_extended_s) + unsigned(carry_s));	
	res_sub_s	<=std_logic_vector(unsigned(a_extended_s) - unsigned(b_extended_s) - unsigned(carry_s));	

	result_o <=	res_add_s(7 downto 0)	when mode_i = "000" else
			res_sub_s(7 downto 0)	when mode_i = "001" else
			a_i or b_i		when mode_i = "010" else
			a_i and b_i		when mode_i = "011" else
			a_i			when mode_i = "100" else
			b_i			when mode_i = "101" else
			"0000000" & comp_s 	when mode_i = "110" else
			"00000000";

	carry_o <= res_add_s(SIZE) when mode_i(0) = '0' else res_sub_s(SIZE);
end behave;
