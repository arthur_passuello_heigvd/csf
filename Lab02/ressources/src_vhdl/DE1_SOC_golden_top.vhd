------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : DE1_SOC_golden_top.vhd
-- Author               : Mike Meury (MIM)
-- Date                 : 09.02.2018
--
-- Context              : SpikeDetection - LABO CSF 2018
--
------------------------------------------------------------------------------------------
-- Description : DE1-SoC golden top for SpikeDetection
--   
--               KEY0 : Enable generator for spikedetection
--
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 1.0    26.02.18    MIM           Creation
--
------------------------------------------------------------------------------------------

---------------------
-- Project library --
---------------------
library work;
use work.log_pkg.all;

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------
-- Entity --
------------
entity DE1_SOC_golden_top is
    port (
        CLOCK_50    : in    std_logic;
        TD_CLK27    : in    std_logic;
        GPIO_0      : inout std_logic_vector(35 downto 0);
        GPIO_1      : inout std_logic_vector(35 downto 0);
        HEX0        : out   std_logic_vector(6 downto 0);
        HEX1        : out   std_logic_vector(6 downto 0);
        HEX2        : out   std_logic_vector(6 downto 0);
        HEX3        : out   std_logic_vector(6 downto 0);
        HEX4        : out   std_logic_vector(6 downto 0);
        HEX5        : out   std_logic_vector(6 downto 0);
        KEY         : in    std_logic_vector(3 downto 0);
        LEDR        : out   std_logic_vector(9 downto 0);
        SW          : in    std_logic_vector(9 downto 0);
        VGA_B       : out   std_logic_vector(7 downto 0);
        VGA_BLANK_N : out   std_logic;
        VGA_CLK     : out   std_logic;
        VGA_G       : out   std_logic_vector(7 downto 0);
        VGA_HS      : out   std_logic;
        VGA_R       : out   std_logic_vector(7 downto 0);
        VGA_SYNC_N  : out   std_logic;
        VGA_VS      : out   std_logic
     --DRAM_ADDR   : out   std_logic_vector(12 downto 0);
     --DRAM_BA     : out   std_logic_vector(1 downto 0);
     --DRAM_CAS_N  : out   std_logic;
     --DRAM_CKE    : out   std_logic;
     --DRAM_CLK    : out   std_logic;
     --DRAM_CS_N   : out   std_logic;
     --DRAM_DQ     : inout std_logic_vector(15 downto 0);
     --DRAM_LDQM   : out   std_logic;
     --DRAM_RAS_N  : out   std_logic;
     --DRAM_UDQM   : out   std_logic;
     --DRAM_WE_N   : out   std_logic 
        );
end entity;  -- DE1_SOC_golden_top

architecture rtl of DE1_SOC_golden_top is

    component VGA_Ctrl is
        port(
            iRed       : in  std_logic_vector(7 downto 0);
            iGreen     : in  std_logic_vector(7 downto 0);
            iBlue      : in  std_logic_vector(7 downto 0);
            oAddress   : out std_logic_vector(21 downto 0);
            oCurrent_X : out std_logic_vector(10 downto 0);
            oCurrent_Y : out std_logic_vector(10 downto 0);
            oRequest   : out std_logic;
            oVGA_R     : out std_logic_vector(7 downto 0);
            oVGA_G     : out std_logic_vector(7 downto 0);
            oVGA_B     : out std_logic_vector(7 downto 0);
            oVGA_HS    : out std_logic;
            oVGA_VS    : out std_logic;
            oVGA_SYNC  : out std_logic;
            oVGA_BLANK : out std_logic;
            oVGA_CLOCK : out std_logic;
            iCLK       : in  std_logic;
            iRST_N     : in  std_logic);
    end component;

    -- User I/O wrapping
    alias user_n_rst_a     : std_logic is KEY(3);
    alias user_n_keys_a    : std_logic_vector(2 downto 0) is KEY(2 downto 0);
    alias user_switchs_a   : std_logic_vector(7 downto 0) is SW(7 downto 0);
    alias user_leds_a      : std_logic_vector(9 downto 0) is LEDR(9 downto 0);
    alias user_n_sevseg5_a : std_logic_vector(6 downto 0) is HEX5;
    alias user_n_sevseg4_a : std_logic_vector(6 downto 0) is HEX4;
    alias user_n_sevseg3_a : std_logic_vector(6 downto 0) is HEX3;
    alias user_n_sevseg2_a : std_logic_vector(6 downto 0) is HEX2;
    alias user_n_sevseg1_a : std_logic_vector(6 downto 0) is HEX1;
    alias user_n_sevseg0_a : std_logic_vector(6 downto 0) is HEX0;

    -- user signals
    signal user_rst_s     : std_logic;
    signal user_keys_s    : std_logic_vector(2 downto 0);
    signal user_sevseg5_s : std_logic_vector(6 downto 0);
    signal user_sevseg4_s : std_logic_vector(6 downto 0);
    signal user_sevseg3_s : std_logic_vector(6 downto 0);
    signal user_sevseg2_s : std_logic_vector(6 downto 0);
    signal user_sevseg1_s : std_logic_vector(6 downto 0);
    signal user_sevseg0_s : std_logic_vector(6 downto 0);
    signal vga_r_s        : std_logic_vector(7 downto 0);
    signal vga_g_s        : std_logic_vector(7 downto 0);
    signal vga_b_s        : std_logic_vector(7 downto 0);
    signal vga_x_s        : std_logic_vector(10 downto 0);
    signal vga_y_s        : std_logic_vector(10 downto 0);

    -- specifics signals
    signal enable_s            : std_logic;
    signal spike_detected_s    : std_logic;
    signal line_bram_s         : std_logic_vector(7 downto 0);
    signal data_spike_s        : std_logic_vector(15 downto 0);
    signal key_sync, key_sync2 : std_logic;
    signal key_rose            : std_logic;

begin

    --------------------
    -- Unused Outputs --
    --------------------
    GPIO_0           <= (others => 'Z');
    GPIO_1           <= (others => 'Z');
    user_n_sevseg0_a <= (others => '1');
    user_n_sevseg1_a <= (others => '0');
    user_n_sevseg2_a <= (others => '1');
    user_n_sevseg3_a <= (others => '0');
    user_n_sevseg4_a <= (others => '1');
    user_n_sevseg5_a <= (others => '0');
    user_leds_a      <= (others => '0');


    -----------------
    -- User Access --
    -----------------
    user_rst_s  <= not user_n_rst_a;
    user_keys_s <= not user_n_keys_a;

    ---------------
    -- Spike top --
    ---------------
    spike_top_inst : entity work.spike_top
        port map (
            -- standard inputs
            clk_i            => TD_CLK27,
            rst_i            => user_rst_s,
            -- specific inputs
            enable_i         => enable_s,
            -- specific outputs
            spike_detected_o => spike_detected_s,
            -- VGA side
            line_vga_i       => line_bram_s,
            data_vga_o       => data_spike_s
            );

    ----------------------
    -- Display instance --
    ----------------------
    display_inst : entity work.display
        port map (
            -- standard inputs
            clk_i           => TD_CLK27,
            rst_i           => user_rst_s,
            -- Spike top interface
            bram_addr_o     => line_bram_s,
            data_i          => data_spike_s,
            -- VGA interface
            vga_red_o       => vga_r_s,
            vga_green_o     => vga_g_s,
            vga_blue_o      => vga_b_s,
            vga_current_x_i => vga_x_s,
            vga_current_y_i => vga_y_s
            );

    --------------------------
    -- VGA Control instance --
    --------------------------
    VGA_Ctrl_inst : VGA_Ctrl
        port map(
            -- Host Side
            iRed       => vga_r_s,
            iGreen     => vga_g_s,
            iBlue      => vga_b_s,
            oCurrent_X => vga_x_s,
            oCurrent_Y => vga_y_s,
            oAddress   => open,
            oRequest   => open,
            -- VGA Side
            oVGA_R     => VGA_R,
            oVGA_G     => VGA_G,
            oVGA_B     => VGA_B,
            oVGA_HS    => VGA_HS,
            oVGA_VS    => VGA_VS,
            oVGA_SYNC  => VGA_SYNC_N,
            oVGA_BLANK => VGA_BLANK_N,
            oVGA_CLOCK => VGA_CLK,
            -- Control Signals
            iCLK       => TD_CLK27,
            iRST_N     => user_n_rst_a
            );

    ------------
    -- Enable --
    ------------
    process(TD_CLK27, user_rst_s)
    begin
        if user_rst_s = '1' then
            enable_s <= '0';
        elsif rising_edge(TD_CLK27) then
            if key_rose = '1' then
                enable_s <= '1';
            elsif spike_detected_s = '1' then
                enable_s <= '0';
            end if;
        end if;
    end process;

    -------------
    -- Synchro --
    -------------
    process(TD_CLK27, user_rst_s)
    begin
        if user_rst_s = '1' then
            key_sync  <= '0';
            key_sync2 <= '0';
        elsif rising_edge(TD_CLK27) then
            key_sync  <= user_keys_s(0);
            key_sync2 <= key_sync;
        end if;
    end process;
    -- rising edge detector
    key_rose <= key_sync and (not key_sync2);

end architecture;  -- rtl
