-------------------------------------------------------------------------------
-- Title      : Testbench for design "generator"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : generator_tb.vhd
-- Author     : mike  <mike@a13pc02>
-- Company    : 
-- Created    : 2018-02-23
-- Last update: 2018-02-26
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2018 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-02-23  1.0      mike    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------

entity generator_tb is

end entity generator_tb;

-------------------------------------------------------------------------------

architecture testbench of generator_tb is

    ---------------
    -- Constants --
    ---------------
    constant CLOCK_PERIOD : time := 10 ns;

    -- component ports
    signal clk_sti         : std_logic;
    signal rst_sti         : std_logic;
    signal enable_sti      : std_logic;
    signal data_obs        : std_logic_vector(15 downto 0);
    signal valid_obs       : std_logic;
    -- simulation signals
    signal end_sim         : boolean               := false;
    -- counter valid signals
    signal counter_valid_s : unsigned(15 downto 0) := (others => '0');

begin  -- architecture testbench

    -- component instantiation
    DUT : entity work.generator
        port map (
            clk_i    => clk_sti,
            rst_i    => rst_sti,
            enable_i => enable_sti,
            data_o   => data_obs,
            valid_o  => valid_obs
            );

    -- clock generation
    clk_proc : process
    begin
        clk_sti <= '1';
        wait for CLOCK_PERIOD/2;
        clk_sti <= '0';
        wait for CLOCK_PERIOD/2;
        if end_sim = true then
            wait;
        end if;
    end process;

    -- waveform generation
    WaveGen_Proc : process
    begin
        rst_sti    <= '1';
        enable_sti <= '0';
        wait for 5*CLOCK_PERIOD;
        rst_sti    <= '0';
        enable_sti <= '1';
        wait until to_integer(counter_valid_s) = 5000;
        enable_sti <= '0';              -- test disable
        wait for 20000*CLOCK_PERIOD;
        -- end of simulation
        end_sim    <= true;
        report "end of simulation" severity note;
        wait;
    end process WaveGen_Proc;

    -- counter valid process
    cpt_valid : process
    begin
        wait until rising_edge(clk_sti);
        if valid_obs = '1' then
            counter_valid_s <= counter_valid_s + 1;
        end if;
    end process;

end architecture testbench;

-------------------------------------------------------------------------------

configuration generator_tb_testbench_cfg of generator_tb is
    for testbench
    end for;
end generator_tb_testbench_cfg;

-------------------------------------------------------------------------------
