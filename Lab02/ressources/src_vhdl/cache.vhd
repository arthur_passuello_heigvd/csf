-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : cache.vhd
-- Description  : Cache used for read sample an memorising the desired window
--
-- Author       : Arthur Passuello
-- Date         : 27.03.18
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       APO    27.03.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mycache is
    generic (
        DATA_WIDTH	: integer := 16;
        ADDR_WIDTH	: integer := 6
        );

    port (
        clk			: in	std_logic;
        data_i		: in	std_logic_vector((DATA_WIDTH-1) downto 0);
        re			: in	std_logic;
        reset_i		: in	std_logic;
        data_o		: out	std_logic_vector((DATA_WIDTH -1) downto 0)
	);
end mycache;

architecture rtl of mycache is

    -- Build a 2-D array type for the RAM
    subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0);
    type memory_t is array(2**ADDR_WIDTH-1 downto 0) of word_t;

    -- Declare the RAM signal.
    signal cache		: memory_t;
	signal rd_idx		: unsigned(ADDR_WIDTH-1 downto 0);
	signal wr_idx		: unsigned(ADDR_WIDTH-1 downto 0);


begin

    process(clk, reset_i)
    begin
    	if(reset_i = '1') then
    		rd_idx	<= (others => '0');
    		wr_idx	<= "000000" - to_unsigned(50, ADDR_WIDTH);
    	elsif(rising_edge(clk)) then
           if (re = '1') then
           		cache(to_integer(rd_idx))	<= data_i;
				rd_idx						<= rd_idx + to_unsigned(1, ADDR_WIDTH);
				wr_idx						<= wr_idx + to_unsigned(1, ADDR_WIDTH);
           end if;
        end if;
    end process;

    data_o	<= cache(to_integer(wr_idx));

end rtl;
