-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : calcul.vhd
-- Description  :
--
-- Author       : Sydney Hauke
-- Date         : 21.03.18
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    26.02.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity detector is
	port(
		clk_i : 			in std_logic;
		reset_i : 		in std_logic;

		en_rd_i :		in std_logic;
		sample_i : 		in std_logic_vector(15 downto 0);

		spike_det_o	:  out std_logic
	);
end entity;

architecture behave of detector is

  signal x_bar_r, x_bar_s    : signed(31 downto 0);
  signal sum_r, sum_s         : signed(31 downto 0);
  signal std_dev_r, std_dev_s    : signed(31 downto 0);
  signal diff_r, diff_s         : signed(31 downto 0);
  signal spike_det_s      : std_logic;
  signal sample_signed  : signed(15 downto 0);
  constant N : signed(sample_i'range) := x"0080";
  constant C2 : signed(sample_i'range) := x"0024";


begin

sample_signed <= signed(sample_i);

  x_bar_s   <= x_bar_r + shift_right(sample_signed, 7) - shift_right(x_bar_r, 7) when en_rd_i = '1' else
              x_bar_r;

 sum_s      <= resize(sample_signed*sample_signed - shift_right(sum_r, 7) + sum_r*sum_r, 32) when en_rd_i = '1' else
					sum_r;

 std_dev_s  <= resize(sum_r - x_bar_r*x_bar_r, 32);

 diff_s     <= resize((sample_signed - x_bar_r)*(sample_signed - x_bar_r), 32);

 spike_det_s<= '1' when diff_r > resize(std_dev_r*C2,32) else '0';

 process(clk_i, reset_i, x_bar_s, sum_s, std_dev_s, diff_s, spike_det_s)
 begin
    if(reset_i = '1') then
      x_bar_r     <= x"00000000";
      sum_r       <= x"00000000";
      std_dev_r   <= x"00000000";
      diff_r      <= x"00000000";
    elsif (rising_edge(clk_i)) then
      x_bar_r     <= x_bar_s;
      sum_r       <= sum_s;
      std_dev_r   <= std_dev_s;
      diff_r      <= diff_s;
    end if;
 end process;

 spike_det_o <= spike_det_s;

end architecture;
