library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 use IEEE.math_real.all;

entity counter is

  port(
  	clk_i		: in  std_logic; 
  	reset_i		: in  std_logic;
  	start_i		: in  std_logic;
  	next_i		: in  std_logic;
   	q_128_o     : out std_logic;
   	q_150_o		: out std_logic
		);
end counter;

architecture behavioral of counter is
	
	signal cpt_pres_s, cpt_fut_s 	: unsigned(7 downto 0);
	signal det_128_s				: std_logic;
	signal det_150_s 				: std_logic;
	constant VAL_MAX            	: natural := 150;
	constant NUM_MIN_SAMPLE     	: natural := 128;

begin

	process(clk_i, reset_i)
	begin
	  	if(reset_i = '1') then
	  		cpt_pres_s	<= (others => '0');
	  	elsif rising_edge(clk_i) then
	  		cpt_pres_s	<= cpt_fut_s;
	  	end if;
	end process;
	
	cpt_fut_s	<= 	(others => '0')	when start_i = '1' else
							(others => '0')	when cpt_pres_s = to_unsigned(VAL_MAX, 8) else
							cpt_pres_s + 1		when next_i = '1' else
							cpt_pres_s;
					
	det_128_s	<= '1' when cpt_pres_s = to_unsigned(NUM_MIN_SAMPLE, 8) else '0';
	det_150_s	<= '1' when cpt_pres_s = to_unsigned(VAL_MAX, 8) else '0';
	
	q_128_o		<= det_128_s;
	q_150_o		<= det_150_s;
	
end architecture;
