-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : spike_detection.vhd
-- Description  :
--
-- Author       : Jean-Patrick Lelynx
-- Date         : 99.99.99
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       JPL    99.99.99           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------
-- Specifics libraries --
-------------------------

------------
-- Entity --
------------
entity spike_detection is
    port (
        -- standard inputs
        clk_i                  : in  std_logic;
        rst_i                  : in  std_logic;
        -- Samples
        sample_i               : in  std_logic_vector(15 downto 0);
        sample_valid_i         : in  std_logic;
        -- Ouputs
        samples_spikes_o       : out std_logic_vector(15 downto 0);
        samples_spikes_valid_o : out std_logic;
        spike_detected_o       : out std_logic
        );
end entity;

------------------
-- Architecture --
------------------
architecture behave of spike_detection is

  component UC is
    port (
         clk_i		: in	std_logic;
 		    reset_i		: in	std_logic;

 		    valid_i		: in	std_logic;
 		    wr_150_i	: in	std_logic;
 		    spike_det_i	: in	std_logic;

 		    samp_128	: in	std_logic;

 		    init_count_o: out	std_logic;
 		    en_rd_o		: out	std_logic;

 		    spike_det_o	: out	std_logic;
 		    sample_val_o: out	std_logic
        );
end component;

component mycache is
    port (
        clk			: in	std_logic;
        data_i		: in	std_logic_vector(15 downto 0);
        re			: in	std_logic;
        reset_i		: in	std_logic;
        data_o		: out	std_logic_vector(15 downto 0)
    );
end component;

component detector is
    port (
    		clk_i : 			in std_logic;
    		reset_i : 		in std_logic;

    		en_rd_i :		in std_logic;
    		sample_i : 		in std_logic_vector(15 downto 0);

    		spike_det_o	:  out std_logic
    );
end component;

component counter is
    port (
        clk_i		: in  std_logic;
      	reset_i		: in  std_logic;
      	start_i		: in  std_logic;
      	next_i		: in  std_logic;
       	q_128_o     : out std_logic;
       	q_150_o		: out std_logic
    );
end component;

  signal sample_is       : std_logic_vector(sample_i'range);
  signal sample_valid_is       : std_logic;
  signal spike_det_os       : std_logic;
  signal sample_valid_os       : std_logic;
  signal sample_os      : std_logic_vector(15 downto 0);
  signal spike_det_s      : std_logic;
  signal q_150_s      : std_logic;
  signal q_128_s      : std_logic;
  signal init_count_s      : std_logic;
  signal en_rd_s      : std_logic;


begin  -- behave

    ----------------------------------------------------------
    -- REMOVE FOLLOWING LINES JUST TO TEST FULL COMPILATION --
    ----------------------------------------------------------
  
  MSS: UC
  port map (
    clk_i		    => clk_i,
    reset_i		    => rst_i,

    valid_i		    => sample_valid_is,
    wr_150_i	    => q_150_s,
    spike_det_i	    => spike_det_s,

    samp_128	    => q_128_s,

    init_count_o    => init_count_s,
    en_rd_o		    => en_rd_s,

    spike_det_o	    => spike_det_os,
    sample_val_o    => sample_valid_os

  );

  CALCUL: detector
  port map (
    clk_i    => clk_i,
		reset_i    => rst_i,

		en_rd_i    => en_rd_s,
		sample_i     => sample_is,

		spike_det_o    => spike_det_s
  );

  CACHE: mycache
  port map (
    clk			    => clk_i,
    data_i		    => sample_is,
    re			    => en_rd_s,
    reset_i		    => rst_i,
    data_o		    => sample_os
  );

  Counter_inst : counter
	port map (
        clk_i    => clk_i,
        reset_i    => rst_i,
        start_i    => init_count_s,
        next_i   => en_rd_s,
        q_128_o => q_128_s,
        q_150_o => q_150_s
	);

    sample_is     <= sample_i;
    sample_valid_is <= sample_valid_i ;
	 
    -- Ouputs
    samples_spikes_o <= sample_os ;
    samples_spikes_valid_o <= sample_valid_os ;
    spike_detected_o <= spike_det_os;

end architecture;  -- behave
