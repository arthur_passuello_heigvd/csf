-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : transformer.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 26.02.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    26.02.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity transformer is
    generic (
        DATA_SIZE_G : integer := 16;
        DATA_OUT_G  : integer := 256
        );
    port (
        data_i : in  std_logic_vector(DATA_SIZE_G-1 downto 0);
        data_o : out std_logic_vector(DATA_OUT_G-1 downto 0)
        );
end entity;

architecture behave of transformer is

    signal data_truncated_s : std_logic_vector(7 downto 0);
    signal output_data_s    : std_logic_vector(DATA_OUT_G-1 downto 0);

begin

    process (data_truncated_s)
        variable temp : std_logic_vector(DATA_OUT_G-1 downto 0);
    begin
        temp := ((0) => '1', others => '0');
        for i in -128 to 127 loop
            if i < signed(data_truncated_s) then
                -- we must left shift
                temp := temp(temp'high-1 downto 0) & '1'; -- fill with 1
            end if;
        end loop;
        output_data_s <= temp;
    end process;

    data_truncated_s <= data_i(data_i'high downto data_i'high-7);  --
                                                                          --keep msb

    data_o <= output_data_s;

end architecture;
