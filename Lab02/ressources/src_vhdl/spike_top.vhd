-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : spike_top.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 26.02.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    26.02.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spike_top is
    --generic (
    --  );
    port (
        -- standard inputs
        clk_i            : in  std_logic;
        rst_i            : in  std_logic;
        -- specific inputs
        enable_i         : in  std_logic;
        -- specific outputs
        spike_detected_o : out std_logic;
        -- VGA side
        line_vga_i       : in  std_logic_vector(7 downto 0);
        data_vga_o       : out std_logic_vector(15 downto 0)
        );
end entity;

architecture behave of spike_top is

    -----------------------
    -- Internals signals --
    -----------------------
    signal data_gen_s       : std_logic_vector(15 downto 0);
    signal data_valid_s     : std_logic;
    signal sample_spike_s   : std_logic_vector(15 downto 0);
    signal write_sample_s   : std_logic;
    signal addr_s           : std_logic_vector(7 downto 0);
    signal counter_sample_s : unsigned(7 downto 0);
    signal enable_rose_s    : std_logic;
    signal enable_reg_s     : std_logic;

begin

    -----------------------------
    -- Instanciation generator --
    -----------------------------
    gen0 : entity work.generator
        port map (
            -- Standard inputs
            clk_i    => clk_i,
            rst_i    => rst_i,
            -- specifics inputs
            enable_i => enable_i,
            -- specifics outputs
            data_o   => data_gen_s,
            valid_o  => data_valid_s
            );

    -----------------------------------
    -- Instanciation Spike Detection --
    -----------------------------------
    spike0 : entity work.spike_detection
        port map (
            -- Standard signals
            clk_i                  => clk_i,
            rst_i                  => rst_i,
            -- Samples
            sample_i               => data_gen_s,
            sample_valid_i         => data_valid_s,
            -- Outputs
            samples_spikes_o       => sample_spike_s,
            samples_spikes_valid_o => write_sample_s,
            spike_detected_o       => spike_detected_o
            );


    -----------------------------
    -- Instanciation Block RAM --
    -----------------------------
    ram0 : entity work.simple_dual_port_ram_single_clock
        generic map (
            DATA_WIDTH => 16,
            ADDR_WIDTH => 8             -- 256 cases
            )
        port map (
            clk   => clk_i,
            raddr => line_vga_i,
            waddr => addr_s,
            data  => sample_spike_s,
            we    => write_sample_s,
            q     => data_vga_o
            );

    ---------------------------------
    -- Counter for address in BRAM --
    ---------------------------------
    process (clk_i, rst_i)
    begin
        if rst_i = '1' then
            counter_sample_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if enable_rose_s = '1' then
                -- reset for next computation
                counter_sample_s <= (others => '0');
            elsif write_sample_s = '1' then
                if counter_sample_s = 2**counter_sample_s'length-1 then
                    counter_sample_s <= (others => '0');
                else
                    counter_sample_s <= counter_sample_s + 1;
                end if;
            end if;
        end if;
    end process;

    addr_s <= std_logic_vector(counter_sample_s);

    --------------------
    -- Synchro enable --
    --------------------
    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            enable_reg_s <= '0';
        elsif rising_edge(clk_i) then
            enable_reg_s <= enable_i;
        end if;
    end process;
    -- rising edge detector
    enable_rose_s <= enable_i and (not enable_reg_s);

end architecture;
