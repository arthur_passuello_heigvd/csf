-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : UC.vhd
	-- Description  :
	--
	-- Author       : Arthur Passuello
	-- Date         : 18.12.17
	-- Version      : 0.0
	--
	-- Dependencies :
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    18.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	-- Numeric
	use ieee.numeric_std.all;

	------------
	-- Entity --
	------------
	entity UC is
		port (
		    clk_i		: in	std_logic;
		    reset_i		: in	std_logic;

		    valid_i		: in	std_logic;
		    wr_150_i	: in	std_logic;
		    spike_det_i	: in	std_logic;

		    samp_128	: in	std_logic;

		    init_count_o: out	std_logic;
		    en_rd_o		: out	std_logic;
			 
		    spike_det_o	: out	std_logic;
		    sample_val_o: out	std_logic
		    );
	end entity UC;

	------------------
	-- Architecture --
	------------------
	architecture struct of UC is

		type state_t is (
			INIT,
			WAIT_SAMPLE_0,
			WAIT_SAMPLE_1,
			WAIT_SAMPLE_2,
			READ_0,
			READ_1,
			INIT_WR,
			RDWR,
			SAMPLE_128,
			WR_150,
			DONE
			);

		signal cur_state, next_state	: state_t;
		signal init_count_s				: std_logic;
		signal en_rd_s					: std_logic;
		signal spike_det_s				: std_logic;
		signal sample_val_s				: std_logic;

	begin

	process(cur_state, valid_i, wr_150_i, samp_128, spike_det_i)
	begin
		init_count_s	<= '0';
		en_rd_s		 	<= '0';
		spike_det_s 	<= '0';
		sample_val_s 	<= '0';
		next_state	<= INIT;
		case cur_state is
			when INIT =>
				init_count_s	<= '1';
				next_state		<= WAIT_SAMPLE_0;
			when WAIT_SAMPLE_0 =>
				if(valid_i = '1') then
					next_state	<= READ_0;
				else
					next_state	<= WAIT_SAMPLE_0;
				end if;
			when WAIT_SAMPLE_1 =>
				if(spike_det_i = '1') then
					next_state	<= INIT_WR;
				elsif(valid_i = '1') then
					next_state	<= READ_1;
				else
					next_state	<= WAIT_SAMPLE_1;
				end if;

			when WAIT_SAMPLE_2 =>
				if(valid_i = '1') then
					next_state	<= RDWR;
				else
					next_state	<= WAIT_SAMPLE_2;
				end if;

			when READ_0 =>
				en_rd_s		<= '1';
				next_state	<= SAMPLE_128;

			when READ_1 =>
				en_rd_s		<= '1';
				next_state	<= WAIT_SAMPLE_1;

			when INIT_WR =>
				init_count_s	<= '1';
				next_state		<= WAIT_SAMPLE_2;

			when RDWR =>
				en_rd_s		<= '1';
				next_state	<= WR_150;

			when SAMPLE_128 =>
				if(samp_128 = '1') then
					next_state	<= WAIT_SAMPLE_1;
				else
					next_state	<= WAIT_SAMPLE_0;
				end if;

			when WR_150 =>
				sample_val_s<= '1';
				if(wr_150_i = '1') then
					next_state	<= DONE;
				else
					next_state	<= WAIT_SAMPLE_2;
				end if;

			when DONE =>
				spike_det_s	<= '1';
				next_state	<= INIT;

		end case;
	end process;

	process(clk_i, reset_i)
	begin
		if(reset_i = '1') then
			cur_state 		<= INIT;
		elsif (rising_edge(clk_i)) then
		   cur_state		<= next_state;
		end if;
	end process;

	init_count_o	<= init_count_s;
	en_rd_o			<= en_rd_s;
	spike_det_o		<= spike_det_s;
	sample_val_o	<= sample_val_s;

end architecture struct;
