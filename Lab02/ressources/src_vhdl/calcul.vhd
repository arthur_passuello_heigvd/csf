-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : calcul.vhd
-- Description  : 
--
-- Author       : Sydney Hauke
-- Date         : 21.03.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    26.02.18           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity calcul is
	port(
		clk_i 		: in std_logic;
		reset_i 	: in std_logic;
		
		en_rd_i 	: in std_logic;
		sample_i	: in std_logic_vector(15 downto 0);
		
		spike_det_o	: out std_logic;
	);
end entity;

architecture behave of calcul is

	signal std_dev, mean 	: signed(15 downto 0);
	signal sum_s		: signed(31 downto 0);

begin


end architecture;
