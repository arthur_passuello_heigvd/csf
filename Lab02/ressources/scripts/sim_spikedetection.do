set NumericStdNoWarnings 1

file delete -force "work"
file mkdir "work"

vlib work
vmap work work

# Project files
vcom -2008 -explicit -novopt ../src_vhdl/log_pkg.vhd
vcom -2008 -explicit -novopt ../src_vhdl/simple_dual_port_ram_single_clock.vhd
vcom -2008 -explicit -novopt ../src_vhdl/spike_detection.vhd
vcom -2008 -explicit -novopt ../src_vhdl/rom.vhd
vcom -2008 -explicit -novopt ../src_vhdl/generator.vhd
vcom -2008 -explicit -novopt ../src_vhdl/cache.vhd
vcom -2008 -explicit -novopt ../src_vhdl/detector.vhd
vcom -2008 -explicit -novopt ../src_vhdl/counter.vhd
vcom -2008 -explicit -novopt ../src_vhdl/UC.vhd
vcom -2008 -explicit -novopt ../src_vhdl/spike_top.vhd

# test bench files
vcom -2008 -explicit -novopt ../src_tb/spike_detection_tb.vhd

# run simulation
vsim -novopt spike_detection_tb

# add all signals
add wave -r *

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

run 10 ns

set StdArithNoWarnings 0
set NumericStdNoWarnings 0

run -all
