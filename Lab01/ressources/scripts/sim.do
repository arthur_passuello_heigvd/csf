#!/usr/bin/tclsh

# Main proc at the end #

#------------------------------------------------------------------------------
proc vhdl_compil { } {
  global Path_VHDL
  global Path_TB
  puts "\nVHDL compilation :"

  vcom $Path_VHDL/alu.vhd
  vcom $Path_TB/alu_tb.vhd
}

#------------------------------------------------------------------------------
proc sim_start { } {

  vsim -t 1ns -novopt -GSIZE=8 work.alu_tb
#  do wave.do
  add wave -r *
  wave refresh
  run -all
}

#------------------------------------------------------------------------------
proc do_all { } {
  vhdl_compil
  sim_start
}

## MAIN #######################################################################

# Compile folder ----------------------------------------------------
if {[file exists work] == 0} {
  vlib work
}

puts -nonewline "  Path_VHDL => "
set Path_VHDL     "../src"
set Path_TB       "../src_tb"

global Path_VHDL
global Path_TB

# start of sequence -------------------------------------------------

if {$argc==1} {
  if {[string compare $1 "all"] == 0} {
    do_all
  } elseif {[string compare $1 "comp_vhdl"] == 0} {
    vhdl_compil
  } elseif {[string compare $1 "sim"] == 0} {
    sim_start
  }

} else {
  do_all
}
