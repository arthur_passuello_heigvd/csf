-------------------------------------------------------------------------------
-- Title      : Testbench for design "spike_top"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : spike_top_tb.vhd
-- Author     : mike  <mike@a13pc02>
-- Company    : 
-- Created    : 2018-02-26
-- Last update: 2018-02-28
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2018 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-02-26  1.0      mike    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------

entity spike_top_tb is

end entity spike_top_tb;

-------------------------------------------------------------------------------

architecture test_bench of spike_top_tb is

    constant CLOCK_PERIOD : time := 10 ns;

    -- component ports
    signal clk_sti            : std_logic;
    signal rst_sti            : std_logic;
    signal enable_sti         : std_logic;
    signal line_vga_sti       : std_logic_vector(7 downto 0);
    signal spike_detected_obs : std_logic;

    signal end_sim : boolean := false;

    signal counter_x, counter_y : unsigned(9 downto 0) := (others => '0');

    signal data_vga_obs : std_logic_vector(15 downto 0);

begin  -- architecture test_bench

    -- component instantiation
    DUT : entity work.spike_top
        port map (
            clk_i            => clk_sti,
            rst_i            => rst_sti,
            enable_i         => enable_sti,
            spike_detected_o => spike_detected_obs,
            line_vga_i       => line_vga_sti,
            data_vga_o       => data_vga_obs
            );

    -- clock generation
    clk_gen : process
    begin
        clk_sti <= '1';
        wait for CLOCK_PERIOD/2;
        clk_sti <= '0';
        wait for CLOCK_PERIOD/2;
        if end_sim = true then
            wait;
        end if;
    end process;

    -- waveform generation
    WaveGen_Proc : process
    begin
        rst_sti    <= '1';
        enable_sti <= '0';
        wait for 5*CLOCK_PERIOD;
        rst_sti    <= '0';
        enable_sti <= '1';
        wait until rising_edge(spike_detected_obs);
        wait for 40*CLOCK_PERIOD;
        enable_sti <= '0';
        report "A spike has been detected" severity note;
        report "end of simulation" severity note;
        end_sim    <= true;
        wait;
    end process WaveGen_Proc;

    -------------------------
    -- Process counter VGA --
    -------------------------
    -- X
    process
    begin
        wait until rising_edge(clk_sti);
        if counter_x = 639 then
            counter_x <= (others => '0');
        else
            counter_x <= counter_x + 1;
        end if;
        if end_sim = true then
            wait;
        end if;
    end process;
    -- Y
    process
    begin
        wait until rising_edge(clk_sti);
        if counter_x = 639 then
            if counter_y = 479 then
                counter_y <= (others => '0');
            else
                counter_y <= counter_y + 1;
            end if;
        end if;
        if end_sim = true then
            wait;
        end if;
    end process;

    -- 9 downto 2 because this is how display plot data (4 equal pixels on
    -- screen for one sample)
    line_vga_sti <= std_logic_vector(counter_y(9 downto 2));

end architecture test_bench;

-------------------------------------------------------------------------------

configuration spike_top_tb_test_bench_cfg of spike_top_tb is
    for test_bench
    end for;
end spike_top_tb_test_bench_cfg;

-------------------------------------------------------------------------------
