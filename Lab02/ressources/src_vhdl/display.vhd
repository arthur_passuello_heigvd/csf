-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : display.vhd
-- Description  : For SpikeDetection
--
-- Author       : Mike Meury
-- Date         : 26.02.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    26.02.18           Creation
-------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-----------------------
-- Project libraries --
-----------------------
library work;
use work.log_pkg.all;

------------
-- Entity --
------------
entity display is
    port (
        -- standard inputs
        clk_i           : in  std_logic;
        rst_i           : in  std_logic;
        -- Spike top interface
        bram_addr_o     : out std_logic_vector(7 downto 0);
        data_i          : in  std_logic_vector(15 downto 0);
        -- VGA interface
        vga_red_o       : out std_logic_vector(7 downto 0);
        vga_green_o     : out std_logic_vector(7 downto 0);
        vga_blue_o      : out std_logic_vector(7 downto 0);
        vga_current_x_i : in  std_logic_vector(10 downto 0);
        vga_current_y_i : in  std_logic_vector(10 downto 0)
        );
end entity;  -- game_display

------------------
-- Architecture --
------------------
architecture behave of display is

    ----------------------
    -- Design constants --
    ----------------------
    constant INACTIVE_COLOR : std_logic_vector(23 downto 0) := x"000000";
    constant ACTIVE_COLOR   : std_logic_vector(23 downto 0) := x"FFFF00";

    -------------------
    -- Clock divisor --
    -------------------
    signal index_s : unsigned(ilogup(480)-1 downto 0);

    -----------------------
    -- Internals signals --
    -----------------------
    signal color_out_s : std_logic_vector(23 downto 0);

    ---------------------------------------
    -- Mask for plotting data vertically --
    ---------------------------------------
    signal data_s : std_logic_vector(479 downto 0);

    signal cpt_prediction_s   : unsigned(10 downto 0);
    signal data_transformed_s : std_logic_vector(255 downto 0);
    signal my_y               : std_logic_vector(10 downto 0);

begin
    ---------------------------------------------------------------------------------
    --                                    VGA
    --------------------------------------------------------------------------------

    ------------------------
    -- Split color in RGB --
    ------------------------
    vga_red_o   <= color_out_s(23 downto 16);
    vga_green_o <= color_out_s(15 downto 8);
    vga_blue_o  <= color_out_s(7 downto 0);

    -- set position for gameoflife premonition
    --display_line_o <= vga_current_y_i(display_line_o'high downto 0);
    --display_row_o  <= vga_current_x_i(display_row_o'high downto 0);

    -- set output value in function of data readed
    index_s                <= resize(to_unsigned(479, vga_current_y_i'length)-unsigned(vga_current_y_i), index_s'length);
    data_s(255 downto 0)   <= data_transformed_s;
    data_s(479 downto 256) <= (others => '0');
    color_out_s            <= INACTIVE_COLOR when data_s(to_integer(index_s)) = '0' else ACTIVE_COLOR;

    cpt_prediction_s <= unsigned(vga_current_x_i) + 1 when unsigned(vga_current_x_i) < 639 else
                        (others => '0');

    bram_addr_o <= std_logic_vector(cpt_prediction_s(9 downto 2));

    -------------------------------------------------------
    -- This component transforme 0 to 255 in one hot bit --
    -------------------------------------------------------
    transf : entity work.transformer
        generic map (
            DATA_SIZE_G => 16,
            DATA_OUT_G  => 256
            )
        port map (
            data_i => data_i,
            data_o => data_transformed_s
            );

end architecture;  -- behave
